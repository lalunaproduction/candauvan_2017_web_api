require 'test_helper'

class Api::ShipTypesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_ship_types_index_url
    assert_response :success
  end

  test "should get show" do
    get api_ship_types_show_url
    assert_response :success
  end

end
