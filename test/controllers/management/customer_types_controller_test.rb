require 'test_helper'

class Management::CustomerTypesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get management_customer_types_index_url
    assert_response :success
  end

  test "should get new" do
    get management_customer_types_new_url
    assert_response :success
  end

  test "should get create" do
    get management_customer_types_create_url
    assert_response :success
  end

  test "should get edit" do
    get management_customer_types_edit_url
    assert_response :success
  end

  test "should get update" do
    get management_customer_types_update_url
    assert_response :success
  end

  test "should get destroy" do
    get management_customer_types_destroy_url
    assert_response :success
  end

end
