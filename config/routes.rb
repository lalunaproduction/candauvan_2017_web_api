Rails.application.routes.draw do
  namespace :api do
    get 'order_histories/index'
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'
  
  namespace :api do
    resources :roles, :session, :ship_types, :profile, :users, :orders, :device_tokens, :order_histories
    
    get '/profile/getProfileByUserId/:user_id' => 'profile#getProfleByUserId', :as => 'getProfileByUId'
    get '/getUserProfile' => 'profile#getUserProfile', :as => 'getUserProfile'
    post '/orders/updateOrderStatus/:id' => 'orders#updateOrderStatus', :as => 'updateOrderStatus'
    post '/orders/updateShipper/:id' => 'orders#updateShipper', :as => 'updateShipper'
    post '/orders/import' => 'orders#import', :as => 'importOrder'
    post '/users/getUserByCB' => 'users#getByChatbot', :as => 'getByChatbot'
    post '/orders/caculatingOrder' => 'orders#caculatingOrder', :as => 'caculatingOrder'
    post '/users/getShipperByFullName' => 'users#getShipperByFullName', :as => 'getShipperByFullName'
    post '/users/getListShipper' => 'users#getListShipper', :as => 'getListShipper'
  end
  
  namespace :management do
    resources :dashboard, :roles, :users, :profiles, :order_statuses, :ship_types, :promotions, :orders, :customer_types, :order_histories, :order_analyzes
    get '/my-profile' => 'profiles#edit', :as => 'my-profile'
    get '/import_order' => 'orders#importView', :as => 'importView'
    get '/addForCustomerView' => 'orders#addForCustomerView', :as => 'addForCustomerView'
    post '/addForCustomer' => 'orders#addForCustomer', :as => 'addForCustomer'
    post '/import' => 'orders#import', :as => 'import'
    patch 'update-profile' => 'profiles#update', :as => 'update-profile'
    post '/dang-ky' => 'users#create', :as => 'dang-ky'
    post '/orders/caculatingOrder' => 'orders#caculatingOrder', :as => 'caculatingOrder'
    post '/orders/updateOrderStatus' => 'orders#updateOrderStatus', :as => 'updateOrderStatus'
    post '/users/getListShipper' => 'users#getListShipper', :as => 'getListShipper'
    post '/users/refreshToken' => 'users#refreshToken', :as => 'refreshToken'
    # resources :order_analyzes do
    #   member do
    #     match 'index', via: [:get, :post]
    #   end
    # end
    
  end
  
  resources :home, :session
  
  get '/dang-xuat' => 'session#destroy', :as => 'dang-xuat'
  get '/dang-nhap' => 'session#new', :as => 'dang-nhap'
  
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
