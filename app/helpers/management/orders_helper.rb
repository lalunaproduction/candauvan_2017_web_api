module Management::OrdersHelper
    def isOwner?(createdBy)
        if createdBy.to_s == current_user.id.to_s
            return true
        else
            return false
        end
    end
    
    def is_member?
        self.current_user.role.name == "Người Dùng"
    end
end
