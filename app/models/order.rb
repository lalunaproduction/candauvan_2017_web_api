class Order < ApplicationRecord
    attr_accessor :receiver_name, :receiver_phone, :receiver_address, :receiver_district, :receiver_city, :package_height, :package_weight, :package_width, :package_note, :sender_name, :sender_phone, :sender_address, :sender_district, :sender_city
    belongs_to :user
    belongs_to :sender
    belongs_to :receiver
    belongs_to :order_status
    belongs_to :ship_type
    belongs_to :package
    belongs_to :order_history
    belongs_to :promotion
    has_attached_file :file
    
    def self.validUserForOrder?(id)
      @user = User.find(id)
      
      if @user.profile.full_name != nil && @user.profile.address != nil && @user.profile.district != nil
        return true
      else
        return false
      end
    end
    
    def self.generateOrderCode
      currOrdCount = Order.all.size + 1
      
      if currOrdCount < 10
        return "10000" + currOrdCount.to_s
      elsif currOrdCount < 100
        return "1000" + currOrdCount.to_s
      elsif currOrdCount < 1000
        return "100" + currOrdCount.to_s
      elsif currOrdCount < 10000
        return "10" + currOrdCount.to_s
      else
        return "1" + currOrdCount.to_s
      end
    end
    
    def self.setOrderHistory(orderId, orderAction, orderContent, orderCreated)
      @orderHis = OrderHistory.new
      
      @orderHis.order_id = orderId
      @orderHis.action = orderAction
      @orderHis.content = orderContent
      @orderHis.created_by = orderCreated
      @orderHis.updated_by = orderCreated
      @orderHis.created_at = DateTime.now
      @orderHis.updated_at = DateTime.now
      @orderHis.delete_flag = 0
      
      @orderHis.save
    end
    
    def self.calculateOrder(shipType)
      if shipType == "1"
        return 10000
      elsif shipType == "2"
        return 20000
      else
        return 30000
      end
    end
end
