class Api::ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionHelper
  
  def verify_authorzation
    if request.headers["Authorization"].nil?
      return render json: {
           "status": "NOK",
           "code": "401",
           "messages":"Truy ván cần phải cung cấp mã xác nhận Token, vui lòng cung cấp mã xác nhận hoặc liên hệ Quản Trị Viên."
          }
    else
      @token = request.headers["Authorization"]
      @user = User.find_by(token: @token)
      
      if @user.nil?
        return render json: {
           "status": "NOK",
           "code": "401",
           "messages":"Mã xác nhận không hợp lệ."
          }
      end
    end
  end
  
end
