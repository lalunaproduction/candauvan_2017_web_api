class Api::DeviceTokensController < Api::ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:create, :updateOrderStatus]
  
  def create
    information = request.raw_post
    data_parsed = JSON.parse(information)
    
    user_id = data_parsed["user_id"]
    token = data_parsed["token"]
    
    @dt = DeviceToken.where(user_id: user_id, token: token, delete_flag: 0)

    if @dt.size == 0 #check if user + token is existed
      @deviceToken = DeviceToken.new
      
      @deviceToken.user_id = user_id
      @deviceToken.token = token
      @deviceToken.created_at = DateTime.now
      @deviceToken.updated_at = DateTime.now
      @deviceToken.delete_flag = 0
      
      if @deviceToken.save
        render json: {
          "status": "OK",
          "code": "200",
          "messages":"Tạo Token thành công",
          "result": @deviceToken.as_json(except: [:delete_flag])
          }
      else
        render json: {
          "status": "NOK",
          "code": "201",
          "messages":"Tạo Token thất bại, vui lòng kiểm tra lại thông tin",
          "result": ""
          }
      end
    else
      render json: {
           "status": "NOK",
           "code": "201",
           "messages":"Thông tin đã tồn tại",
           "result": ""
          }
    end
  end
end
