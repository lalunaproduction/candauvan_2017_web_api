class Api::OrdersController < Api::ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:create, :updateOrderStatus, :import, :caculatingOrder]
  
  before_filter :verify_authorzation
  before_filter :verify_content_type, :only => [:create, :updateOrderStatus]
  before_filter :validate_order, :only => [:create]
  
  
  def index
    if !params[:order_code].nil?
      @orders = Order.where(order_code: params[:order_code], delete_flag: 0).order(id: :desc).first
      
      if @orders.created_by != @user.id.to_s && @user.id == 3
        return render json: {
         "status": "NOK",
         "code": "201",
         "messages":"Đơn hàng không tồn tại."
        }
      end
    elsif !params[:created_at].nil?
      if params[:skip].nil? || params[:take].nil?
        return render json: {
           "status": "NOK",
           "code": "201",
           "messages":"Truy vấn không hợp lệ, vui lòng kiểm tra lại."
          } 
      end
      @date = Date.parse(params[:created_at].to_s)
      @orders = Order.where(delete_flag: 0, created_by: @user.id.to_s).where(:created_at => @date.beginning_of_day..@date.end_of_day).order(id: :desc).offset(params[:skip]).limit(params[:take])
      #@orders = @orders.limit(params[:take]).offset(params[:skip])
    else
      if params[:skip].nil? || params[:take].nil?
        return render json: {
           "status": "NOK",
           "code": "201",
           "messages":"Truy vấn không hợp lệ, vui lòng kiểm tra lại."
          } 
      end
      if @user.role_id == 3
        @orders = Order.where(created_by: @user.id.to_s, delete_flag: 0).order(id: :desc).offset(params[:skip]).limit(params[:take])
      else
        @orders = Order.where(delete_flag: 0).order(id: :desc).offset(params[:skip]).limit(params[:take])
      end
      
      if !params[:orderStatus].nil?
        if params[:orderStatus] == "Nok"
          @orders = @orders.where.not(order_status_id: 7).where.not(order_status_id: 11).where.not(order_status_id: 12)
        else 
          @orders = @orders.where(order_status_id: 7).or(@orders.where(order_status_id: 11)).or(@orders.where(order_status_id: 12))
          
        end
      end
    end
    
    render json: {
           "status": "OK",
           "code": "200",
           "messages":"Danh sách Đơn Hàng",
           "result": @orders.as_json(except: [:receiver_id, :created_by, :sender_id, :delete_flag, :order_status_id, :package_id, :ship_type_id, :updated_by, :updated_at, :delete_flag], 
                include: {
                  ship_type: {
                    except: [:created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                  },
                  package: {
                    except: [:created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                  },
                  order_status: {
                    except: [:created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                  },
                  receiver: {
                    except: [:created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                  },
                  sender: {
                    except: [:created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                  },
                  promotion: {
                    except: [:created_by, :updated_by, :created_at, :updated_at, :delete_flag, :discount_from, :discount_to, :count, :description, :discount]
                  }
               })
          }
  end

  def show
    
  end

  def create
    if Order.validUserForOrder?(@user.id)
      
      from = generateDistrictFromAddress(@data_parsed["sender_address"]+",Đà Nẵng,Việt Nam")
      to = generateDistrictFromAddress(@data_parsed["receiver_address"]+",Đà Nẵng,Việt Nam")
    
      fromlsData = from.split(/,/)
      tolsData = to.split(/,/)
      
      if fromlsData.length == 4
        fromAddress = fromlsData[0].nil? ? "" : fromlsData[0].to_s.strip
        fromWard = "";
        fromDistrict = fromlsData[1].nil? ? "" : fromlsData[1].to_s.strip.gsub('Q. ','')
        fromCity = fromlsData[2].nil? ? "" : fromlsData[2].to_s.strip
        fromCountry = fromlsData[3].nil? ? "" : fromlsData[3].to_s.strip
      else
        fromAddress = fromlsData[0].nil? ? "" : fromlsData[0].to_s.strip
        fromWard = fromlsData[1].nil? ? "" : fromlsData[1].to_s.strip
        fromDistrict = fromlsData[2].nil? ? "" : fromlsData[2].to_s.strip.gsub('Q. ','')
        fromCity = fromlsData[3].nil? ? "" : fromlsData[3].to_s.strip
        fromCountry = fromlsData[4].nil? ? "" : fromlsData[4].to_s.strip
      end
      
      if tolsData.length == 4
        toAddress = tolsData[0].nil? ? "" : tolsData[0].to_s.strip
        toWard = "";
        toDistrict = tolsData[1].nil? ? "" : tolsData[1].to_s.strip.gsub('Q. ','')
        toCity = tolsData[2].nil? ? "" : tolsData[2].to_s.strip
        toCountry = tolsData[3].nil? ? "" : tolsData[3].to_s.strip
      else
        toAddress = tolsData[0].nil? ? "" : tolsData[0].to_s.strip
        toWard = tolsData[1].nil? ? "" : tolsData[1].to_s.strip
        toDistrict = tolsData[2].nil? ? "" : tolsData[2].to_s.strip.gsub('Q. ','')
        toCity = tolsData[3].nil? ? "" : tolsData[3].to_s.strip
        toCountry = tolsData[4].nil? ? "" : tolsData[4].to_s.strip
      end
      
      @sender                           = Sender.new
      @sender.name                      = @data_parsed["sender_name"]
      @sender.phone                     = @data_parsed["sender_phone"]
      @sender.address                   = @data_parsed["sender_address"]
      @sender.district                  = fromDistrict
      @sender.city                      = fromCity
      @sender.created_by                = @user.name #it must be logged in user
      @sender.created_at                = DateTime.now
      @sender.updated_by                = @user.name #itmust be logged in user
      @sender.updated_at                = DateTime.now
      @sender.delete_flag               = 0
      
      @receiver                         = Receiver.new
      @receiver.name                    = @data_parsed["receiver_name"]
      @receiver.phone                   = @data_parsed["receiver_phone"]
      @receiver.address                 = @data_parsed["receiver_address"]
      @receiver.district                = toDistrict
      @receiver.city                    = toCity
      @receiver.created_by              = @user.name #it must be logged in user
      @receiver.created_at              = DateTime.now
      @receiver.updated_by              = @user.name #itmust be logged in user
      @receiver.updated_at              = DateTime.now
      @receiver.delete_flag             = 0
      
      @package              = Package.new
      @package.height       = @data_parsed["package_height"]
      @package.width        = @data_parsed["package_width"]
      @package.weight       = @data_parsed["package_weight"]
      @package.note         = @data_parsed["package_note"].nil? ? "" : @data_parsed["package_note"]
      @package.created_by   = @user.name #it must be logged in user
      @package.created_at   = DateTime.now
      @package.updated_by   = @user.name #itmust be logged in user
      @package.updated_at   = DateTime.now
      @package.delete_flag  = 0
      
      #save @receiver info
      if @receiver.save && @package.save && @sender.save
        
        ship_type_id = ShipType.find_by(name: @data_parsed["ship_type"]).id
        
        @order = Order.new
        @order.order_code       = Order.generateOrderCode
        @order.sender_id        = @sender.id #must be logged id
        @order.receiver_id      = @receiver.id
        @order.shipper_id       = 0 #it mean no shipper
        @order.package_id       = @package.id
        @order.ship_type_id     = ship_type_id
        @order.customer_code    = @data_parsed["customer_code"]
        @order.cod              = @data_parsed["cod"].to_i
        @order.order_status_id  = 1
        @order.promotion_id     = @promote.nil? ? 0 : @promote.id
        @order.created_by       = @user.id #it must be logged in user
        @order.created_at       = DateTime.now
        @order.updated_by       = @user.id #itmust be logged in user
        @order.updated_at       = DateTime.now
        @order.delete_flag      = 0
        
        if !@data_parsed["promotion_code"].nil? && @data_parsed["promotion_code"] == "CDVDEFTWE-2017"
          @order.total = 20000
        elsif !@data_parsed["promotion_code"].nil? && @data_parsed["promotion_code"] == "CDVDEFTEN-2017"
          @order.total = 10000
        elsif @data_parsed["promotion_code"].nil? && @data_parsed["promotion_code"] == "CDVDEFTHI-2017"
          @order.total = 15000
        elsif @data_parsed["promotion_code"].nil? && @data_parsed["promotion_code"] == "CDVDEF25K-2017"
          @order.total = 25000
        elsif @data_parsed["promotion_code"].nil? && @data_parsed["promotion_code"] == "CDVDEF30K-2017"
          @order.total = 30000
        elsif @data_parsed["promotion_code"].nil? && @data_parsed["promotion_code"] == "CDVDEF35K-2017"
          @order.total = 35000
        elsif @data_parsed["promotion_code"].nil? && @data_parsed["promotion_code"] == "CDVDEF40K-2017"
          @order.total = 40000
        elsif @data_parsed["promotion_code"].nil? && @data_parsed["promotion_code"] == "CDVDEF45K-2017"
          @order.total = 45000
        elsif @data_parsed["promotion_code"].nil? && @data_parsed["promotion_code"] == "CDVDEF50K-2017"
          @order.total = 50000
        else
          # from = @sender.address+","+@sender.district+","+@sender.city+",Việt Nam"
          # to = @receiver.address+","+@receiver.district+","+@receiver.city+",Việt Nam"
          distanceValue, distanceKm = generateDistanceBetween2points(from, to)
          baseRate = getBaseRate(ship_type_id.to_s)
          
          @order.total = caculated(baseRate, distanceValue, ship_type_id.to_s)
        end
        
        if @order.save
          
          #save order history
          Order.setOrderHistory(@order.id, @order.order_status_id, "",@user.profile.full_name)
          
          render json: {
           "status": "OK",
           "code": "200",
           "messages":"Tạo đơn hàng Thành Công",
           "distance": distanceKm,
           "result": @order.as_json(except: [:id, :shipper_id, :promotion_id, :sender_id, :user_id, :order_status_id, :receiver_id, :package_id, :ship_type_id, :created_by, :updated_by, :created_at, :updated_at, :delete_flag],
                include: {
                  sender: {
                    except: [:id, :created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                  },
                  receiver: {
                    except: [:id, :created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                  },
                  package: {
                    except: [:id, :created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                  },
                  order_status: {
                    except: [:id, :created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                  },
                  promotion: {
                    except: [:id, :updated_at, :updated_by, :created_at, :created_by, :delete_flag, :count, :discount_from, :discount_to, :description, :discount]
                  }
               })
          }
        else
          render json: {
           "status": "OK",
           "code": "202",
           "messages":"Tạo đơn hàng Thất bại, Vui lòng kiểm tra lại thông tin đơn hàng",
           "result": ""
          }
        end
      else
        render json: {
           "status": "OK",
           "code": "202",
           "messages":"Tạo đơn hàng Thất bại, Vui lòng kiểm tra lại thông tin gói hàng"
          }
      end
    else
      render json: {
           "status": "OK",
           "code": "202",
           "messages":"Tài khoản chưa hợp lệ, cần được cập nhật đầy đủ thông tin trước khi tạo Đơn Hàng"
          }
    end
  end
  
  def updateOrderStatus
    @order = Order.find(params[:id])
    
    information = request.raw_post
    data_parsed = JSON.parse(information)
    

    if Order.validUserForOrder?(@user.id)
      @order.order_status_id = data_parsed["order_status_id"].to_i
      
      if @order.order_status_id == 3 || @order.order_status_id == 5 || @order.order_status_id == 7 || @order.order_status_id == 11 || @order.order_status_id == 12
        @order.shipper_id = data_parsed["shipper_id"].to_i
      end
      
      @order.updated_at = DateTime.now
      @order.updated_by = @user.id
      
      if @order.save
        
        #save order history
        if @order.order_status_id == 3 || @order.order_status_id == 5 || @order.order_status_id == 7 || @order.order_status_id == 11 || @order.order_status_id == 12
          Order.setOrderHistory(@order.id, @order.order_status_id, data_parsed["content"] + " - " + User.find(@order.shipper_id).profile.full_name,@user.profile.full_name)
        else
          Order.setOrderHistory(@order.id, @order.order_status_id, data_parsed["content"],@user.profile.full_name)
        end
        
        render json: {
             "status": "OK",
             "code": "200",
             "messages":"Cập nhật Đơn Hàng thành công",
             "result": @order.as_json(except: [:delete_flag, :user_id, :order_status_id, :receiver_id, :package_id, :ship_type_id, :created_by, :updated_by, :created_at, :updated_at, :delete_flag],
              include: {
                    package: {
                      except: [:created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                    },
                    receiver: {
                      except: [:created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                    },
                    user: {
                      except: [:encrypt_password, :salt, :created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                    },
                    order_status: {
                      except: [:created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                    }
                 })
            }
      else
        render json: {
           "status": "OK",
           "code": "201",
           "messages":"Cập nhật đơn hàng Lỗi, vui lòng kiểm tra lại dữ liệu.",
           "result": ""
          }  
      end
    else
      render json: {
           "status": "OK",
           "code": "201",
           "messages":"Tài khoản cần được cập nhật đầy đủ thông tin trước khi tạo Đơn Hàng",
           "result": ""
          }
    end
  end
  
  def caculatingOrder
    
    information = request.raw_post
    data_parsed = JSON.parse(information)
    
    senderAddress    = data_parsed["sender_address"]
    receiverAddress  = data_parsed["receiver_address"]
    ship_type     = data_parsed["ship_type"].to_s
    
    ship_type_id = ShipType.find_by(name: ship_type).id
    
    from = generateDistrictFromAddress(senderAddress+",Đà Nẵng,Việt Nam")
    to = generateDistrictFromAddress(receiverAddress+",Đà Nẵng,Việt Nam")
    
    fromlsData = from.split(/,/)
    tolsData = to.split(/,/)
    
    distanceValue, distanceKm = generateDistanceBetween2points(from, to)
    baseRate = getBaseRate(ship_type_id)
    
    promotionCode = data_parsed["promotion_code"]
    
    @price = nil
    @msg = nil
    
    unless promotionCode == ""
      if promotionCode == "CDVDEFTWE-2017"
        @price = 20000
        @msg = "OK"
      elsif promotionCode == "CDVDEFTEN-2017"
        @price = 10000
        @msg = "OK"
      elsif promotionCode == "CDVDEFTHI-2017"
        @price = 15000
        @msg = "OK"
      elsif promotionCode == "CDVDEF25K-2017"
        @price = 25000
        @msg = "OK"
      elsif promotionCode == "CDVDEF30K-2017"
        @price = 30000
        @msg = "OK"
      elsif promotionCode == "CDVDEF35K-2017"
        @price = 35000
        @msg = "OK"
      elsif promotionCode == "CDVDEF40K-2017"
        @price = 40000
        @msg = "OK"
      elsif promotionCode == "CDVDEF45K-2017"
        @price = 45000
        @msg = "OK"
      elsif promotionCode == "CDVDEF50K-2017"
        @price = 50000
        @msg = "OK"
      else
        #check promotion is valid
        @promote = Promotion.where("code = ? AND count > 0",promotionCode).first
        
        unless @promote.nil? #valid
          discount = @promote.discount
          @price = caculated(discount, distanceValue, ship_type_id)
          @msg = "OK"
        else
          @price = caculated(baseRate, distanceValue, ship_type_id)
          @msg = "Failed Promotion"
        end
      end
    else
      @price = caculated(baseRate, distanceValue, ship_type_id)
      @msg = "OK"
    end
    
    if fromlsData.length == 4
      fromAddress = fromlsData[0].nil? ? "" : fromlsData[0].to_s.strip
      fromWard = "";
      fromDistrict = fromlsData[1].nil? ? "" : fromlsData[1].to_s.strip.gsub('Q. ','')
      fromCity = fromlsData[2].nil? ? "" : fromlsData[2].to_s.strip
      fromCountry = fromlsData[3].nil? ? "" : fromlsData[3].to_s.strip
    else
      fromAddress = fromlsData[0].nil? ? "" : fromlsData[0].to_s.strip
      fromWard = fromlsData[1].nil? ? "" : fromlsData[1].to_s.strip
      fromDistrict = fromlsData[2].nil? ? "" : fromlsData[2].to_s.strip.gsub('Q. ','')
      fromCity = fromlsData[3].nil? ? "" : fromlsData[3].to_s.strip
      fromCountry = fromlsData[4].nil? ? "" : fromlsData[4].to_s.strip
    end
    
    if tolsData.length == 4
      toAddress = tolsData[0].nil? ? "" : tolsData[0].to_s.strip
      toWard = "";
      toDistrict = tolsData[1].nil? ? "" : tolsData[1].to_s.strip.gsub('Q. ','')
      toCity = tolsData[2].nil? ? "" : tolsData[2].to_s.strip
      toCountry = tolsData[3].nil? ? "" : tolsData[3].to_s.strip
    else
      toAddress = tolsData[0].nil? ? "" : tolsData[0].to_s.strip
      toWard = tolsData[1].nil? ? "" : tolsData[1].to_s.strip
      toDistrict = tolsData[2].nil? ? "" : tolsData[2].to_s.strip.gsub('Q. ','')
      toCity = tolsData[3].nil? ? "" : tolsData[3].to_s.strip
      toCountry = tolsData[4].nil? ? "" : tolsData[4].to_s.strip
    end
    
    render json: {
        "status" =>  "OK",
        "code" => "200",
        "messages": "Tạm Tính Đơn Hàng",
        "result" => {
          "from" => {
            "address" => fromAddress,
            "wards" => fromWard,
            "district" => fromDistrict,
            "city" => fromCity,
            "country" => fromCountry
          },
          "to" => {
            "address" => toAddress,
            "wards" => toWard,
            "district" => toDistrict,
            "city" => toCity,
            "country" => toCountry
          },
          "distanceValue" => distanceValue,
          "distanceText" => distanceKm,
          "promotion" => promotionCode,
          "totalPrice" => @price
      }
    }
  end
  
  def updateShipper
  end
  
  def verify_content_type
    if request.headers["Content-Type"].nil?
      return render json: {
           "status": "NOK",
           "code": "401",
           "messages":"Truy ván cần phải cung cấp Content-Type, vui lòng cập nhật lại truy vấn."
          }
    else
      unless request.headers["Content-Type"].include?("application/json")
        return render json: {
           "status": "NOK",
           "code": "401",
           "messages":"Kiểu dữ liệu truy vấn phải là json, vui lòng cập nhật lại truy vấn."
          }
      end
    end
  end
  
  def validate_order
    information = request.raw_post
    @data_parsed = JSON.parse(information)
    
    #Kiểm tra thông tin người gởi, người nhận, thông tin gói hàng
    if @data_parsed["sender_name"].nil? || @data_parsed["sender_name"]  == "" || @data_parsed["sender_address"].nil? || @data_parsed["sender_address"] == "" || @data_parsed["sender_phone"].nil? || @data_parsed["sender_phone"] == ""
      return render json: {
           "status": "NOK",
           "code": "202",
           "messages":"Thông tin Người Gởi chưa hợp lệ, vui lòng cập nhật lại."
          }
    elsif @data_parsed["receiver_name"].nil? || @data_parsed["receiver_name"] == "" || @data_parsed["receiver_address"].nil? || @data_parsed["receiver_address"] == "" || @data_parsed["receiver_phone"].nil? || @data_parsed["receiver_phone"] == ""
      return render json: {
           "status": "NOK",
           "code": "202",
           "messages":"Thông tin Người Nhận chưa hợp lệ, vui lòng cập nhật lại."
          }
    elsif @data_parsed["package_height"].nil? || @data_parsed["package_height"] == "" || @data_parsed["package_weight"].nil? || @data_parsed["package_weight"] == "" || @data_parsed["package_width"].nil? || @data_parsed["package_width"] == ""
      return render json: {
           "status": "NOK",
           "code": "202",
           "messages":"Thông tin Gói Hàng chưa hợp lệ, vui lòng cập nhật lại."
          }
    end

    #kiểm tra kiểu vận chuyển có hợp lệ hay không
    if @data_parsed["ship_type"].nil? || @data_parsed["ship_type"] == ""
      return render json: {
           "status": "NOK",
           "code": "202",
           "messages":"Thông tin Kiểu giao hàng chưa hợp lệ, vui lòng cập nhật lại."
          }
    else
      @shipTypes = ShipType.where(name: @data_parsed["ship_type"], delete_flag: 0)
      
      if @shipTypes.size == 0
        return render json: {
           "status": "NOK",
           "code": "202",
           "messages":"Thông tin Kiểu giao hàng chưa hợp lệ, vui lòng cập nhật lại."
          }
      end
      
    end

    #kiểm tra COD có hợp lệ hay không
    unless @data_parsed["cod"].nil?
      unless @data_parsed["cod"].to_i.is_a? Numeric
        return render json: {
             "status": "NOK",
             "code": "202",
             "messages":"Thông tin COD chưa hợp lệ, vui lòng cập nhật lại. COD là kiểu số vd: 100000"
            }
      end
    end
    
    #kiểm tra Mã khuyến mãi có hợp lệ hay không    
    unless @data_parsed["promotion_code"].nil? || @data_parsed["promotion_code"] == ""
      @promote = Promotion.where("code = ? AND count > 0",@data_parsed["promotion_code"]).first
      if @promote.nil?
        return render json: {
             "status": "NOK",
             "code": "202",
             "messages":"Mã khuyến mãi không tồn tại, hoặc đã hết hạn sử dụng, vui lòng cập nhật lại."
            }
      end
    end
  end
  
  def generateDistanceBetween2points(from, to)
    #GG API KEY - khovideos1@gmail.com
    @API_KEY = "AIzaSyB2O1_Piue0e0oEOUNFI3ONKb5O6dodPXE"
    @MAP_API = "https://maps.googleapis.com/maps/api/directions/json?"
    
    prms = URI.encode_www_form([["origin", from], ["destination", to],["key", @API_KEY],["mode","json"],["language","vi"]])
    @MAP_API << prms
    
    response = HTTParty.get(@MAP_API)
    response.parsed_response


    if response["status"] == "OK"
      distanceValue = response["routes"][0]["legs"][0]["distance"]["value"].to_i
      distanceKm = response["routes"][0]["legs"][0]["distance"]["text"].to_s
    end
    
    return distanceValue, distanceKm
  end
  
  def generateDistrictFromAddress(addr)
    @GEO_API = "https://maps.googleapis.com/maps/api/geocode/json?"
    prms = URI.encode_www_form([["address", addr]])
    
    @GEO_API << prms
    
    response = HTTParty.get(@GEO_API)
    response.parsed_response
    
    if response["status"] == "OK"
      address = response["results"][0]["formatted_address"].to_s
    end
    
    return address
  end
  
  def getBaseRate(ship_type_id)
    if ship_type_id.to_s == "1" #giao thường
      baseRate = 10000
    elsif ship_type_id.to_s == "2" #giao nhanh
      baseRate = 20000
    else #giao siêu tốc
      baseRate = 30000
    end
    return baseRate
  end

  def caculated(baseRate, km, type)
      value = 0
      km = km / 1000.0
      
      if km % 1 != 0
        km = km.to_i + 1
      end
      
      if km > 10
        if type.to_s == "1"
          value = baseRate + ((km - 10) * 3000)
        elsif type.to_s == "2"
          value = baseRate + ((km - 10) * 4000)
        else
          value = baseRate + ((km - 10) * 5000)
        end
      else
        value = baseRate
      end
      return value
  end
  
  def import
    
    uploaded_file = params[:file]
    fileName = uploaded_file.original_filename
    #check the upload file is excel file
    if fileName.end_with?(".xls") || fileName.end_with?(".xlsx")
      #starting to read xls file and upload to server
      file_path      = params[:file].tempfile
      file_extension = File.extname params[:file].original_filename
      if file_extension.eql? ".xlsx"
        file_data       = Roo::Spreadsheet.open(file_path)
        @data            = file_data.sheet 0
        @first_row_index = 1+8
        @originalRow     = 1+8
      else
        file_data       = Spreadsheet.open(file_path)
        @data            = file_data.worksheet 0
        @first_row_index = 0+8
        @originalRow     = 0+8
      end
      
      @isContinue = true
      
      while @data.row(@first_row_index)[0].nil? == false && @isContinue do
        dataRow = @data.row(@first_row_index)
        @isContinue = validateOrder?(dataRow)
        @first_row_index += 1
      end
      
      unless @isContinue
        return render json: {
           "status": "NOK",
           "code": "202",
           "messages":@errMsg
          }
      end
      
      while @data.row(@originalRow)[0].nil? == false && @isContinue do
        dataRow = @data.row(@originalRow)
        @isContinue = addOrder?(dataRow)
        @originalRow += 1
      end
      
      if @isContinue
        return render json: {
         "status": "OK",
         "code": "200",
         "messages":'Import Danh Sách Đơn Hàng Thành Công.'
        }
      else
        return render json: {
         "status": "NOK",
         "code": "202",
         "messages":@errMsg
        }
      end
    else
      return render json: {
       "status": "NOK",
       "code": "202",
       "messages":'File không hợp lệ, Vui lòng kiểm tra lại. File phải là định dạng Excel.'
      }
    end
  end
  
  def validateOrder?(data)
    num = data[0] 
    senderName = data[1].to_s
    senderAddress = data[2].to_s
    senderDistrict = data[3].to_s
    senderPhone = data[4].to_s
    receiverName = data[5].to_s
    receiverAddress = data[6].to_s
    receiverDistrict = data[7].to_s
    receiverPhone = data[8].to_s
    packageHeight = data[9].to_s
    packageWidth = data[10].to_s
    packageWeight = data[11].to_s
    packageNote = data[12].nil? ? "" : data[12].to_s
    shipType = data[13].to_s
    cod = data[14].nil? ? 0 : data[14].to_i
    promotion = data[15].nil? ? "" : data[15].to_s
    
    #validate sender information
    if senderName == ""
      @errMsg = 'Tên người gởi của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
      return false
    end
    
    if senderAddress == ""
      @errMsg = 'Địa chỉ người gởi của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
      return false
    end
    
    if senderPhone == ""
      @errMsg = 'SĐT người gởi của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
      return false
    end
    
    if senderDistrict == ""
      @errMsg = 'Tên Quận người gởi của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
      return false
    end
    
    #validate receiver information
    if receiverName == ""
      @errMsg = 'Tên người nhận của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
      return false
    end
    
    if receiverAddress == ""
      @errMsg = 'Địa chỉ người nhận của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
      return false
    end
    
    if receiverPhone == ""
      @errMsg = 'SĐT người nhận của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
      return false
    end
    
    if receiverDistrict == ""
      @errMsg = 'Tên Quận người nhận của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
      return false
    end
    
    #validate package information
    if packageHeight == ""
      @errMsg = 'Chiều cao gói hàng của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
      return false
    end
    
    if packageWidth == ""
      @errMsg = 'Chiều rộng gói hàng của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
      return false
    end
    
    if packageWeight == ""
      @errMsg = 'Cân Nặng gói hàng của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
      return false
    end
    
    #validate order information
    
    if shipType == ""
      @errMsg = 'Kiểu vận chuyển của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
      return false
    elsif shipType.downcase != "chuyển thường" && shipType.downcase != "chuyển nhanh" && shipType.downcase != "chuyển siêu tốc"
      @errMsg = 'Kiểu vận chuyển của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
      return false
    end
    
    unless Order.validUserForOrder?(@user.id)
      @errMsg = 'Vui lòng cập nhật đầy đủ thông tin tại <a href="/management/my-profile"> ĐÂY</a>, trước khi tạo đơn hàng.'
      return false
    end
    
    unless promotion == ""
      #check promotion is valid
      @promote = Promotion.where("code = ? AND count > 0",promotion).first
      unless @promote.nil? #valid
        @price = @promote.discount
        @promotionId = @promote.id
      else
        @errMsg = 'Mã khuyến mãi của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
    end
    
    return true
  end
  
  def addOrder?(data)
    num = data[0] 
    senderName = data[1].to_s
    senderAddress = data[2].to_s
    senderDistrict = data[3].to_s
    senderPhone = data[4].to_s
    receiverName = data[5].to_s
    receiverAddress = data[6].to_s
    receiverDistrict = data[7].to_s
    receiverPhone = data[8].to_s
    packageHeight = data[9].to_s
    packageWidth = data[10].to_s
    packageWeight = data[11].to_s
    packageNote = data[12].nil? ? "" : data[12].to_s
    shipType = data[13].to_s
    cod = data[14].nil? ? 0 : data[14].to_i
    promotion = data[15].nil? ? "" : data[15].to_s
    ship_type_id = ShipType.find_by(name: shipType).id
    
    unless promotion == ""
      #check promotion is valid
      @promote = Promotion.where("code = ? AND count > 0",promotion).first
      unless @promote.nil? #valid
        @price = @promote.discount
        @promotionId = @promote.id
      else
        @errMsg = 'Mã khuyến mãi của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
    else
      @price = 0
      @promotionId = 0
    end
    
    @sender = Sender.new
    @sender.name = senderName
    @sender.phone = senderPhone
    @sender.address = senderAddress
    @sender.district = senderDistrict
    @sender.city = "Đà Nẵng"
    @sender.created_by = @user.name #it must be logged in user
    @sender.created_at = DateTime.now
    @sender.updated_by = @user.name #itmust be logged in user
    @sender.updated_at = DateTime.now
    @sender.delete_flag = 0
    
    @receiver = Receiver.new
    @receiver.name = receiverName
    @receiver.phone = receiverPhone
    @receiver.address = receiverAddress
    @receiver.district = receiverDistrict
    @receiver.city = "Đà Nẵng"
    @receiver.created_by = @user.name #it must be logged in user
    @receiver.created_at = DateTime.now
    @receiver.updated_by = @user.name #itmust be logged in user
    @receiver.updated_at = DateTime.now
    @receiver.delete_flag = 0
    
    @package = Package.new
    @package.height = packageHeight
    @package.width = packageWidth
    @package.weight = packageWeight
    @package.note = packageNote
    @package.created_by = @user.name #it must be logged in user
    @package.created_at = DateTime.now
    @package.updated_by = @user.name #itmust be logged in user
    @package.updated_at = DateTime.now
    @package.delete_flag = 0
    
    #save @receiver, @sender, @package info
    if @receiver.save && @package.save && @sender.save
      @order = Order.new
      @order.order_code = Order.generateOrderCode
      @order.sender_id = @sender.id #thí is sender id
      @order.receiver_id = @receiver.id
      @order.shipper_id = 0 #it mean no shipper
      @order.package_id = @package.id
      @order.ship_type_id = ship_type_id
      @order.order_status_id = 1
      @order.promotion_id = @promotionId
      @order.cod = cod
      @order.created_by = @user.id #it must be logged in user
      @order.created_at = DateTime.now
      @order.updated_by = @user.id #itmust be logged in user
      @order.updated_at = DateTime.now
      @order.delete_flag = 0
      
      if promotion == "CDVDEFTWE-2017"
        @order.total = 20000
      elsif promotion == "CDVDEFTEN-2017"
        @order.total = 10000
      elsif promotion == "CDVDEFTHI-2017"
        @order.total = 15000
      elsif promotion == "CDVDEF25K-2017"
        @order.total = 25000
      elsif promotion == "CDVDEF30K-2017"
        @order.total = 30000
      elsif promotion == "CDVDEF35K-2017"
        @order.total = 35000
      elsif promotion == "CDVDEF40K-2017"
        @order.total = 40000
      elsif promotion == "CDVDEF45K-2017"
        @order.total = 45000
      elsif promotion == "CDVDEF50K-2017"
        @order.total = 50000
      else
        from = @sender.address+","+@sender.district+",Đà Nẵng,Việt Nam"
        to = @receiver.address+","+@receiver.district+",Đà Nẵng, Việt Nam"
        
        distanceValue, distanceKm = generateDistanceBetween2points(from, to)
        baseRate = getBaseRate(ship_type_id.to_s)
        
        if @promotionId == 0
          @order.total = caculated(baseRate, distanceValue, ship_type_id.to_s)
        else
          @order.total = caculated(@price, distanceValue, ship_type_id.to_s)
        end
      end
      
      if @order.save
        #add history
        Order.setOrderHistory(@order.id, "Mới Tạo", "",@order.created_by)
        return true
      else
       @errMsg = "Tạo đơn hàng Thất bại, vui lòng kiểm tra lại thông tin, hoặc liên hệ người quản lý."
        return false
      end
    end
  end
end
