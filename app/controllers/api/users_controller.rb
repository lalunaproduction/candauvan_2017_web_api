class Api::UsersController < Api::ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:create, :getByChatbot, :getListShipper, :getShipperByFullName]
  before_filter :verify_authorzation, :only => [:show]
  
  def index
     @users = User.all
     render json: {
           "status": "OK",
           "code": "200",
           "messages":"Danh sách người dùng",
           "result": @users.as_json(except: [:encrypt_password, :salt, :delete_flag], 
                include: {
                  profile: {
                    except: [:delete_flag]
                  }
               })
          }
  end
  
  def show
    @user = User.find(params[:id])
    render json: {
         "status": "OK",
         "code": "200",
         "messages":"Danh sách người dùng",
         "result": @user.as_json(except: [:encrypt_password, :salt, :delete_flag], 
              include: {
                profile: {
                  except: [:delete_flag]
                }
             })
        }
  end
  
  def create
    information = request.raw_post
    data_parsed = JSON.parse(information)
    
    @user = User.new()
    @user.name = data_parsed["name"]
    @user.email = data_parsed["email"]
    @user.phone = data_parsed["phone"]
    @user.role_id = 3
    @user.password = data_parsed["password"]
    @user.created_by = @user.name
    @user.updated_by = @user.name
    @user.provider = "Mobile"
    @user.delete_flag = 0
    
    #TODO: 
    #kiem tra user da ton tai hay chua (email/sdt)
    if User.isUserValid?(@user.name, @user.phone)
      if @user.save
        #create profile for user
        @profile = Profile.new
        @profile.user_id = @user.id
        @profile.delete_flag = 0
        @profile.created_by = @user.name
        @profile.updated_by = @user.name
      
        if @profile.save
          render json: {
           "status": "OK",
           "code": "200",
           "messages":"Đăng ký thành công",
           "result": @user.as_json(except: [:encrypt_password, :salt, :delete_flag], 
                include: {
                  profile: {
                    except: [:delete_flag]
                  }
               })
          }
        else
          render json: {
           "status": "OK",
           "code": "201",
           "messages":"Đăng ký Thất bại, Vui lòng kiểm tra lại thông tin đăng ký",
           "result": @user.as_json(except: [:encrypt_password, :salt, :delete_flag], 
                include: {
                  profile: {
                    except: [:delete_flag]
                  }
               })
          }
        end
      end
    else
      render json: {
           "status": "OK",
           "code": "201",
           "messages":"Đăng ký Thất bại, Email hoặc SĐT này đã được đăng ký",
           "result": ""
          }
    end
  end
  
  def getByChatbot
    information = request.raw_post
    data_parsed = JSON.parse(information)
    
    @user = User.where(email: data_parsed["email"], phone: data_parsed["phone"]).first
    
    unless @user.nil?
      return render json: {
           "status": "OK",
           "code": "200",
           "messages":"Thông tin người dùng",
           "result": @user.as_json(except: [:encrypt_password, :salt, :delete_flag], 
                include: {
                  profile: {
                    except: [:delete_flag]
                  }
               })
          }
    else
      return render json: {
           "status": "NOK",
           "code": "202",
           "messages":"Không có người dùng nào trùng với thông tin này."
          }
    end
  end
  
  def getListShipper
    @users = User.where(role_id: 2)
    
    unless @users.nil?
      return render json: {
           "status": "OK",
           "code": "200",
           "messages":"Danh sách shipper",
           "result": @users.as_json(except: [:encrypt_password, :salt, :delete_flag], 
                include: {
                  profile: {
                    except: [:delete_flag]
                  }
               })
          }
    else
      return render json: {
           "status": "NOK",
           "code": "202",
           "messages":"Không có người dùng nào trùng với thông tin này."
          }
    end
  end
  
  def getShipperByFullName
    information = request.raw_post
    data_parsed = JSON.parse(information)
    
    @profile = Profile.find_by(full_name: data_parsed["full_name"])
    @user = User.find(@profile.user_id)
    
    unless @user.nil?
      return render json: {
           "status": "OK",
           "code": "200",
           "messages":"Thông Tin shipper",
           "result": @user.as_json(except: [:encrypt_password, :salt, :delete_flag], 
                include: {
                  profile: {
                    except: [:delete_flag]
                  }
               })
          }
    else
      return render json: {
           "status": "NOK",
           "code": "202",
           "messages":"Không có người dùng nào trùng với thông tin này."
          }
    end
    
  end
    
end
