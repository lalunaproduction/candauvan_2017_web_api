class Api::OrderHistoriesController < Api::ApplicationController
  before_filter :verify_authorzation
  
  def index
    if params[:order_code].nil?
      @orders = nil
    else
      order = Order.find_by(order_code: params[:order_code])
      unless order.nil?
        @orders = OrderHistory.where(order_id: order.id)
      end
    end
    
    @lsItem = []
    
    unless @orders.nil? 
      @orders.each do |i|
        obj = {}
        obj["time"] = i.created_at.strftime("%d/%m/%Y\n%H:%M:%S")
        obj["title"] = OrderStatus.find(i.action.to_i).name
        obj["description"] = i.content.strip
        if obj["title"] == "Mới Tạo"
          obj["circleColor"] = "#8e44ad"
        elsif obj["title"] == "Xác Nhận"
          obj["circleColor"] = "#81C784"
        elsif obj["title"] == "Đã Lấy Hàng"
          obj["circleColor"] = "#e67e22"
        elsif obj["title"] == "Đang Giao Hàng"
          obj["circleColor"] = "#FFD740"
        elsif obj["title"] == "Giao Hàng Thành Công" || obj["title"] == "Hoàn Tất"
          obj["circleColor"] = "#16a085"
        elsif obj["title"] == "Chờ Giao Lại" || obj["title"] == "Huỷ"
          obj["circleColor"] = "#c0392b"
        else
          obj["circleColor"] = "#2980b9"
        end
        obj["lineColor"] = "#2980b9"
        @lsItem.push(obj)
      end
    end
    
    render json: {
       "status": "OK",
       "code": "200",
       "messages":"Lịch sử đơn hàng",
       "result": @lsItem.as_json()
      }
    
  end
end
