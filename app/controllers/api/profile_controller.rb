class Api::ProfileController < Api::ApplicationController
  skip_before_filter :verify_authenticity_token, :only => :update
  before_filter :verify_authorzation
  
  def show
    @profile = Profile.where(user_id: params[:user_id], delete_flag: 0)
    @user = User.find(params[:user_id])
    
    render json: {
     "status": "OK",
     "code": "200",
     "messages":"Lấy thông tin cá nhân thành công",
     "result": @user.as_json(except: [:encrypt_password, :salt, :delete_flag], 
          include: {
            profile: {
              except: [:delete_flag]
            }
         })
    }
  end
  
  def update
    information = request.raw_post
    data_parsed = JSON.parse(information)
  
    @user = User.find(params[:id])
    
    @profile = Profile.where(user_id: data_parsed["user_id"]).first
    @profile.full_name = data_parsed["full_name"]
    @profile.address = data_parsed["address"]
    @profile.district = data_parsed["district"]
    @profile.city = data_parsed["city"]
    @profile.updated_by = @user.name
    @profile.updated_at = DateTime.now
    
    @user.email = data_parsed["email"]
    @user.phone = data_parsed["phone"]
    @user.updated_by = @user.name
    @user.updated_at = DateTime.now
    
    if @profile.save && @user.save
      render json: {
       "status": "OK",
       "code": "200",
       "messages":"Cập nhật Thông tin cá nhân thành công",
       "result":  @user.as_json(except: [:encrypt_password, :salt, :delete_flag], 
          include: {
            profile: {
              except: [:delete_flag]
            }
         })
      }
    else
      render json: {
       "status": "NOK",
       "code": "401",
       "messages":"Cập nhật Thông tin cá nhân Lỗi, Vui lòng kiểm tra lại",
       "result": nil
      }
    end
    
  end

  def getByUserId
    
    @profile = Profile.where(user_id: params[:user_id], delete_flag: 0).first
    
    render json: {
     "status": "OK",
     "code": "200",
     "messages":"",
     "result": @profile
    }
  end
  
  def getUserProfile
    
    @user = User.find_by(token: @token)
    
    unless @user.nil?
        return render json: {
         "status": "OK",
         "code": "200",
         "messages":"Lấy thông tin cá nhân thành công",
         "result": @user.as_json(except: [:encrypt_password, :salt, :delete_flag], 
              include: {
                profile: {
                    except: [:delete_flag]
                },
                role: {
                    except: [:delete_flag]
                }
             })
        }
    else 
        return render json: {
         "status": "NOK",
         "code": "201",
         "messages":"Lấy thông tin cá nhân Thất bại",
         "result": ""
        }
    end
  end
end
