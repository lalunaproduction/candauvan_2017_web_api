class Api::RolesController < Api::ApplicationController
  before_action :set_post, only: [:show, :update, :destroy]
  
  def create
    @role = Role.new(role_params)
    
    if @role.save
        render json: {
         "status": :create,
         "code": "200",
         "messages":"",
         "result": @role
        }
    else
        render json: {
         "status": :unprocessable_entity,
         "code": "500",
         "messages": @role.errors,
         "result": ""
        }
    end
  end

  def index
    @roles = Role.where(delete_flag: 0).all
    render json: {
     "status": "OK",
     "code": "200",
     "messages":"",
     "result": @roles
    }
  end

  def show
    render json: {
     "status": "OK",
     "code": "200",
     "messages":"",
     "result": @role
    }
  end
  
  def update
      if @role.update(role_params)
          render json: {
           "status": "OK",
           "code": "200",
           "messages":"",
           "result": @role
          }
      else
          render json: {
           "status": :unprocessable_entity,
           "code": "500",
           "messages": @role.errors,
           "result": ""
          }
      end
  end
  
  def destroy
  end
  
  private
    
    def set_post
      @role = Role.find(params[:id])    
    end
    
    
    def post_params
        params.require(:post).permit!
    end
end
