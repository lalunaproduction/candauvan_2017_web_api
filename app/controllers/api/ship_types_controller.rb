class Api::ShipTypesController < Api::ApplicationController
  before_filter :verify_authorzation
  
  def index
    @ship_types = ShipType.where(delete_flag: 0).all
    render json: {
     "status": "OK",
     "code": "200",
     "messages":"Danh sách kiểu vận chuyển",
     "result": @ship_types.as_json(except: [:delete_flag, :updated_by, :updated_at, :delete_flag])
    }
  end

  def show
    @ship_type = ShipType.where(delete_flag: 0).find(params[:id])
    render json: {
     "status": "OK",
     "code": "200",
     "messages":"Thông tin kiểu vận chuyển",
     "result": @ship_type.as_json(except: [:delete_flag, :updated_by, :updated_at, :delete_flag])
    }
  end
end
