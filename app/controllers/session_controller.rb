class SessionController < ApplicationController
  def new
  end

  def create
    user = User.authenticate(params[:name], params[:password])
    
    if user.nil?
      flash.now[:error] = "Invalid username/password"
    else
      sign_in user
      redirect_to management_orders_path
    end
    
  end

  def destroy
    sign_out
    redirect_to root_path
  end
end
