class Management::OrderStatusesController < Management::ApplicationController
  before_filter :verify_logged
  
  def new
    @order_status = OrderStatus.new
  end

  def create
    @order_status = OrderStatus.new(order_status_params)
    @order_status.created_by = "Admin"
    @order_status.updated_by = "Admin"
    @order_status.delete_flag = 0
    
    if @order_status.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to management_order_statuses_path
    else
      render 'new'
    end
  end

  def index
    @order_statuses = OrderStatus.where(delete_flag: 0).all
  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
  end
  
  private
    def order_status_params
      params.require(:order_status).permit(:name, :description, :created_by, :updated_by, :delete_flag)
    end
end
