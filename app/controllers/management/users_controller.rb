class Management::UsersController < Management::ApplicationController
  before_filter :verify_logged, :except => [:create, :getListShipper]
  
  def new
    @user = User.new
  end

  def create
    
    if params["email"].nil?
      @user = User.new(user_params)
      @user.created_by = current_user.name
      @user.updated_by = current_user.name
    else
      @user = User.new
      @user.email = params["email"]
      @user.phone = params["phone"]
      @user.password = params["password"]
      if @user.role_id.nil?
        @user.role_id = 3
      end
      @user.created_by = "system"
      @user.updated_by = "system"
    end
    
    #generate Token
    
    o = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
    @user.token = (0...32).map { o[rand(o.length)] }.join
    
    @user.provider = "Website"
    @user.delete_flag = 0
    
    #TODO: 
    #kiem tra user da ton tai hay chua (name/email/sdt)
    if User.isUserValid?(@user.name, @user.phone)
      if @user.save
        #create profile for user
        @profile = Profile.new
        @profile.user_id = @user.id
        @profile.delete_flag = 0
        @profile.created_by = "system"
        @profile.updated_by = "system"
        
        if @profile.save
          flash[:success] = 'Tạo mới thành công'
          sign_in @user
          if params["email"].nil?
            redirect_to management_users_path
          else
            redirect_to management_my_profile_path
          end
        end
      else
        render 'new'
      end
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    @user.email = params["user"]["email"]
    @user.phone = params["user"]["phone"]
    @user.name =  params["user"]["name"]
    @user.role_id = params["user"]["role_id"]
    @user.customer_type_id = params["user"]["customer_type_id"]
    
    if @user.save
      flash[:success] = 'Cập nhật thành công'
      redirect_to management_users_path
    else
      flash[:success] = 'Cập nhật Thất bại'
      render 'edit'
    end
  end

  def index
    @users = User.where(delete_flag: 0).all
  end

  def show
  end

  def destroy
  end
  
  def getListShipper
    @users = User.includes(:profile).where(delete_flag: 0, role_id: 2)
    
    render json: {
      "resultData" => {
        "listShipper" => @users.as_json(except: [:created_by, :updated_by, :created_at, :updated_at, :delete_flag],
                include: {
                  profile: {
                    except: [:created_by, :updated_by, :created_at, :updated_at, :delete_flag]
                  }
                }
              )
      }
    }
    
  end
  
  def refreshToken
    userId = params["id"]
    user = User.find(userId)
    o = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
    user.token = (0...32).map { o[rand(o.length)] }.join
    
    if user.save
      render json: {
        "resultData" => {
          "msg" =>  "ok",
          "token" => user.token
        }
      }
    end
    
  end
  
  private
    def user_params
      params.require(:user).permit(:name, :role_id, :userstatus_id, :customer_type_id, :email, :phone, :password, :token)
    end
end
