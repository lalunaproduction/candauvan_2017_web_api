class Management::ProfilesController < Management::ApplicationController
  before_filter :verify_logged 
  
  def edit
    @user = User.find_by(id: current_user.id)
  end

  def update
    
    @profile = Profile.find_by(user_id: current_user.id)
    @profile.full_name = params[:profile][:full_name]
    @profile.address = params[:profile][:address]
    @profile.district = params[:profile][:district]
    @profile.city = "Đà Nẵng"
    @profile.updated_at = DateTime.now
    @profile.updated_by = current_user.name
    
    @user = User.find_by(id: current_user.id)
    @user.email = params[:user][:email]
    @user.phone = params[:user][:phone]
    
    if @profile.save && @user.save
      flash[:success] = 'Cập nhật thông tin thành công'
      redirect_to management_my_profile_path
    else
      flash[:error] = 'Cập nhật Thất bại, vui lòng kiểm tra lại'
      redirect_to management_my_profile_path
    end
    
  end
  
end
