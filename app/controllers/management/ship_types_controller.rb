class Management::ShipTypesController < Management::ApplicationController
  before_filter :verify_logged
  
  def new
    @ship_type = ShipType.new
  end

  def create
    @ship_type = ShipType.new(ship_type_params)
    @ship_type.created_by = "Admin"
    @ship_type.updated_by = "Admin"
    @ship_type.delete_flag = 0
    
    if @ship_type.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to management_ship_types_path
    else
      render 'new'
    end
  end

  def index
    @ship_types = ShipType.where(delete_flag: 0).all
  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
  end
  
  private
    def ship_type_params
      params.require(:ship_type).permit(:name, :description, :created_by, :updated_by, :delete_flag)
    end
end
