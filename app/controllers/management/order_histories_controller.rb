class Management::OrderHistoriesController < Management::ApplicationController
  def index
    if params[:orderId].nil?
      @orders = nil
    else
      order = Order.find_by(order_code: params[:orderId])
      unless order.nil?
        @orders = OrderHistory.where(order_id: order.id)
      end
    end
    
  end
end
