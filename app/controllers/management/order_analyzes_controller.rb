class Management::OrderAnalyzesController < Management::ApplicationController
  def index
    unless params[:shipperId].nil?
      
      unless params[:fromDate].nil?
        @fromDate = params[:fromDate]
      end
      
      unless params[:toDate].nil?
        @toDate = params[:toDate]
      end
      
      fDate = @fromDate.to_date
      fDate = (fDate-1).to_s + " 17:00:00"
      
      tDate = @toDate.to_date
      tDate = tDate.to_s + " 16:59:59"
      
      @orders = Order.where("updated_at": fDate..tDate).order(id: :desc)
      
      if params[:orderStatusId] != "0"
        @orders = @orders.where('order_status_id': params[:orderStatusId].to_i)
      end
      
      if params[:shipperId] != "0"
        @orders = @orders.where('shipper_id': params[:shipperId].to_i)
      end
      
      if params[:senderId] != "0"
        cusName = User.find(params[:senderId]).profile.full_name
        @lsCustomerId = Sender.where(name: cusName, delete_flag: 0).select(:id)
        @orders = @orders.where(sender_id: @lsCustomerId)
      end
      
      @totalCod = @orders.sum("cod")
      @totalFee = @orders.sum("total")
      
    end
    
    #Order.where('updated_at': "2017-07-22 17:00:00".."2017-07-23 16:59:59")
    
    @users = User.includes(:profile).where(delete_flag: 0, role_id: 2)
    @customer = User.includes(:profile).where(delete_flag: 0, role_id: 3)
    @orderStatus = OrderStatus.where(delete_flag: 0)
  end
end
