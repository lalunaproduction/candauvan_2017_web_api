class Management::PromotionsController < Management::ApplicationController
  before_filter :verify_logged
  
  def new
    @promotion = Promotion.new
  end

  def create
    @promotion = Promotion.new(promotion_params)
    @promotion.created_by = "Admin"
    @promotion.updated_by = "Admin"
    @promotion.delete_flag = 0
    
    if @promotion.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to management_promotions_path
    else
      render 'new'
    end
  end

  def index
    @promotions = Promotion.where(delete_flag: 0).all
  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
  end
  
  private
    def promotion_params
      params.require(:promotion).permit(:code, :discount, :discount_from, :discount_to, :count, :description, :created_by, :updated_by, :delete_flag)
    end
end
