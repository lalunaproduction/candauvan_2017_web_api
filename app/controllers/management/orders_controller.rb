class Management::OrdersController < Management::ApplicationController
  skip_before_filter :verify_authenticity_token, :only => :import
  before_filter :verify_logged
  require 'fcm'
  require 'roo'
  
  def new
    unless Order.validUserForOrder?(current_user.id)
      flash[:userInfo] = 'Vui lòng cập nhật đầy đủ thông tin tại <a href="/management/my-profile"> ĐÂY</a>, trước khi tạo đơn hàng.'
    end
    
    @user = User.find(current_user.id)
    @order = Order.new
    
    @order.sender_name = @user.profile.full_name
    @order.sender_address = @user.profile.address
    @order.sender_phone = @user.phone
    @order.sender_district = @user.profile.district
  end
  
  def addForCustomerView
    @user = User.find(current_user.id)
    @order = Order.new
  end
  
  def addForCustomer
    @price = nil
    @promotionId = 0;
    
    unless Order.validUserForOrder?(params["order"]["sender_id"].to_i)
      flash[:userInfo] = 'Vui lòng cập nhật đầy đủ thông tin tại <a href="/management/my-profile"> ĐÂY</a>, trước khi tạo đơn hàng.'
      redirect_to management_orders_path 
    end
    
    unless params["order"]["promotion_id"] == ""
      #check promotion is valid
      @promote = Promotion.where("code = ? AND count > 0",params["order"]["promotion_id"]).first
      
      unless @promote.nil? #valid
        @price = @promote.discount
        @promotionId = @promote.id
        @msg = "OK"
      else
        flash[:error] = 'Mã khuyến mãi không hợp lệ, Vui lòng kiểm tra lại.'
        return redirect_to new_management_order_path
      end
    end
    
    isReceiver = ''
    
    if (params["order"]["receiver_address"][0].downcase == 'k') 
      isReceiver = params["order"]["receiver_address"][0]
      receiverA = params["order"]["receiver_address"]
      receiverA[0] = ''
    end
    
    to = generateDistrictFromAddress(params["order"]["receiver_address"]+",Đà Nẵng,Việt Nam")
    
    unless isReceiver == ''
      params["order"]["receiver_address"].prepend(isReceiver)
    end
    
    tolsData = to.split(/,/)
    
    if tolsData.length == 4
      toAddress = tolsData[0].nil? ? "" : tolsData[0].to_s.strip
      toWard = "";
      toDistrict = tolsData[1].nil? ? "" : tolsData[1].to_s.strip.gsub('Q. ','')
      toCity = tolsData[2].nil? ? "" : tolsData[2].to_s.strip
      toCountry = tolsData[3].nil? ? "" : tolsData[3].to_s.strip
    else
      toAddress = tolsData[0].nil? ? "" : tolsData[0].to_s.strip
      toWard = tolsData[1].nil? ? "" : tolsData[1].to_s.strip
      toDistrict = tolsData[2].nil? ? "" : tolsData[2].to_s.strip.gsub('Q. ','')
      toCity = tolsData[3].nil? ? "" : tolsData[3].to_s.strip
      toCountry = tolsData[4].nil? ? "" : tolsData[4].to_s.strip
    end
    
    @senderUser = User.find(params["order"]["sender_id"].to_i)
    
    @sender = Sender.new
    @sender.name = @senderUser.profile.full_name
    @sender.phone = @senderUser.phone
    @sender.address = @senderUser.profile.address
    @sender.district = @senderUser.profile.district
    @sender.city = "Đà Nẵng"
    @sender.created_by = @senderUser.name #it must be logged in user
    @sender.created_at = DateTime.now
    @sender.updated_by = @senderUser.name #itmust be logged in user
    @sender.updated_at = DateTime.now
    @sender.delete_flag = 0
    
    @receiver = Receiver.new
    @receiver.name = params["order"]["receiver_name"]
    @receiver.phone = params["order"]["receiver_phone"]
    if toWard == ""
      @receiver.address = params["order"]["receiver_address"]
    else
      @receiver.address = params["order"]["receiver_address"]+", "+toWard
    end
    @receiver.district = toDistrict
    @receiver.city = "Đà Nẵng"
    @receiver.created_by = @senderUser.name #it must be logged in user
    @receiver.created_at = DateTime.now
    @receiver.updated_by = @senderUser.name #itmust be logged in user
    @receiver.updated_at = DateTime.now
    @receiver.delete_flag = 0
    
    @package = Package.new
    @package.height = params["order"]["package_height"]
    @package.width = params["order"]["package_width"]
    @package.weight = params["order"]["package_weight"]
    @package.note = params["order"]["package_note"]
    @package.created_by = current_user.name #it must be logged in user
    @package.created_at = DateTime.now
    @package.updated_by = current_user.name #itmust be logged in user
    @package.updated_at = DateTime.now
    @package.delete_flag = 0
    
    #save @receiver, @sender, @package info
    if @receiver.save && @package.save && @sender.save
      @order = Order.new
      @order.order_code = Order.generateOrderCode
      @order.sender_id = @sender.id #thí is sender id
      @order.receiver_id = @receiver.id
      @order.shipper_id = 0 #it mean no shipper
      @order.package_id = @package.id
      @order.customer_code = params["order"]["customer_code"].strip
      @order.payer = params["order"]["payer"].strip
      @order.ship_type_id = params["order"]["ship_type_id"]
      @order.order_status_id = 1
      @order.promotion_id = @promotionId
      @order.cod = params["order"]["cod"].to_i
      @order.created_by = @senderUser.id #it must be logged in user
      @order.created_at = DateTime.now
      @order.updated_by = @senderUser.id #itmust be logged in user
      @order.updated_at = DateTime.now
      @order.delete_flag = 0
      
      if params["order"]["promotion_id"] == "CDVDEFTWE-2017"
        @order.total = 20000
      elsif params["order"]["promotion_id"] == "CDVDEFTEN-2017"
        @order.total = 10000
      elsif params["order"]["promotion_id"] == "CDVDEFTHI-2017"
        @order.total = 15000
      elsif params["order"]["promotion_id"] == "CDVDEF25K-2017"
        @order.total = 25000
      elsif params["order"]["promotion_id"] == "CDVDEF30K-2017"
        @order.total = 30000
      elsif params["order"]["promotion_id"] == "CDVDEF35K-2017"
        @order.total = 35000
      elsif params["order"]["promotion_id"] == "CDVDEF40K-2017"
        @order.total = 40000
      elsif params["order"]["promotion_id"] == "CDVDEF45K-2017"
        @order.total = 45000
      elsif params["order"]["promotion_id"] == "CDVDEF50K-2017"
        @order.total = 50000
      else
        from = @sender.address+","+@sender.district+",Đà Nẵng,Việt Nam"
        distanceValue, distanceKm = generateDistanceBetween2points(from, to)
        baseRate = getBaseRate(params["order"]["ship_type_id"].to_s)
        
        if @promotionId == 0
          @order.total = caculated(baseRate, distanceValue, params["order"]["ship_type_id"].to_s)
        else
          @order.total = caculated(@price, distanceValue, params["order"]["ship_type_id"].to_s)
        end
      end
      
      if @order.save
        msg = "Tạo mới đơn hàng " + @order.order_code + " thành công."
        flash[:success] = msg
        
        #add history
        Order.setOrderHistory(@order.id, 1, "Tạo mới đơn hàng",User.find(@order.created_by).profile.full_name)
        
        redirect_to management_orders_path
      else
        flash[:error] = "Tạo đơn hàng Thất bại, vui lòng kiểm tra lại thông tin, hoặc liên hệ người quản lý."
        render new
      end
    end
  end

  def create
    @price = nil
    @promotionId = 0;
    unless Order.validUserForOrder?(current_user.id)
      flash[:userInfo] = 'Vui lòng cập nhật đầy đủ thông tin tại <a href="/management/my-profile"> ĐÂY</a>, trước khi tạo đơn hàng.'
      redirect_to management_orders_path 
    end
    
    unless order_params["promotion_id"] == ""
      #check promotion is valid
      @promote = Promotion.where("code = ? AND count > 0",order_params["promotion_id"]).first
      
      unless @promote.nil? #valid
        @price = @promote.discount
        @promotionId = @promote.id
        @msg = "OK"
      else
        flash[:error] = 'Mã khuyến mãi không hợp lệ, Vui lòng kiểm tra lại.'
        return redirect_to new_management_order_path
      end
    end
    
    isSender = ''
    isReceiver = ''
    
    if (order_params["sender_address"][0].downcase == 'k')
      isSender = order_params["sender_address"][0]
      senderA = order_params["sender_address"]
      senderA[0] = ''
    end
    
    if (order_params["receiver_address"][0].downcase == 'k') 
      isReceiver = order_params["receiver_address"][0]
      receiverA = order_params["receiver_address"]
      receiverA[0] = ''
    end

    from = generateDistrictFromAddress(order_params["sender_address"]+",Đà Nẵng,Việt Nam")
    to = generateDistrictFromAddress(order_params["receiver_address"]+",Đà Nẵng,Việt Nam")
    
    unless isSender == ''
      order_params["sender_address"].prepend(isSender)
    end
    
    unless isReceiver == ''
      order_params["receiver_address"].prepend(isReceiver)
    end
    
    fromlsData = from.split(/,/)
    tolsData = to.split(/,/)
    
    if fromlsData.length == 4
      fromAddress = fromlsData[0].nil? ? "" : fromlsData[0].to_s.strip
      fromWard = "";
      fromDistrict = fromlsData[1].nil? ? "" : fromlsData[1].to_s.strip.gsub('Q. ','')
      fromCity = fromlsData[2].nil? ? "" : fromlsData[2].to_s.strip
      fromCountry = fromlsData[3].nil? ? "" : fromlsData[3].to_s.strip
    else
      fromAddress = fromlsData[0].nil? ? "" : fromlsData[0].to_s.strip
      fromWard = fromlsData[1].nil? ? "" : fromlsData[1].to_s.strip
      fromDistrict = fromlsData[2].nil? ? "" : fromlsData[2].to_s.strip.gsub('Q. ','')
      fromCity = fromlsData[3].nil? ? "" : fromlsData[3].to_s.strip
      fromCountry = fromlsData[4].nil? ? "" : fromlsData[4].to_s.strip
    end
    
    if tolsData.length == 4
      toAddress = tolsData[0].nil? ? "" : tolsData[0].to_s.strip
      toWard = "";
      toDistrict = tolsData[1].nil? ? "" : tolsData[1].to_s.strip.gsub('Q. ','')
      toCity = tolsData[2].nil? ? "" : tolsData[2].to_s.strip
      toCountry = tolsData[3].nil? ? "" : tolsData[3].to_s.strip
    else
      toAddress = tolsData[0].nil? ? "" : tolsData[0].to_s.strip
      toWard = tolsData[1].nil? ? "" : tolsData[1].to_s.strip
      toDistrict = tolsData[2].nil? ? "" : tolsData[2].to_s.strip.gsub('Q. ','')
      toCity = tolsData[3].nil? ? "" : tolsData[3].to_s.strip
      toCountry = tolsData[4].nil? ? "" : tolsData[4].to_s.strip
    end
    
    @sender = Sender.new
    @sender.name = order_params["sender_name"].strip
    @sender.phone = order_params["sender_phone"].strip
    
    if fromWard == ""
      @sender.address = order_params["sender_address"].strip
    else
      @sender.address = order_params["sender_address"]+", "+fromWard
    end
    
    @sender.district = fromDistrict
    @sender.city = "Đà Nẵng"
    @sender.created_by = current_user.name #it must be logged in user
    @sender.created_at = DateTime.now
    @sender.updated_by = current_user.name #itmust be logged in user
    @sender.updated_at = DateTime.now
    @sender.delete_flag = 0
    
    @receiver = Receiver.new
    @receiver.name = order_params["receiver_name"].strip
    @receiver.phone = order_params["receiver_phone"].strip
    if toWard == ""
      @receiver.address = order_params["receiver_address"].strip
    else
      @receiver.address = order_params["receiver_address"].strip+", "+toWard
    end
    @receiver.district = toDistrict
    @receiver.city = "Đà Nẵng"
    @receiver.created_by = current_user.name #it must be logged in user
    @receiver.created_at = DateTime.now
    @receiver.updated_by = current_user.name #itmust be logged in user
    @receiver.updated_at = DateTime.now
    @receiver.delete_flag = 0
    
    @package = Package.new
    @package.height = order_params["package_height"].strip
    @package.width = order_params["package_width"].strip
    @package.weight = order_params["package_weight"].strip
    @package.note = order_params["package_note"].strip
    @package.created_by = current_user.name #it must be logged in user
    @package.created_at = DateTime.now
    @package.updated_by = current_user.name #itmust be logged in user
    @package.updated_at = DateTime.now
    @package.delete_flag = 0
    
    #save @receiver, @sender, @package info
    if @receiver.save && @package.save && @sender.save
      @order = Order.new
      @order.customer_code = order_params["customer_code"].strip
      @order.payer = order_params["payer"].strip
      @order.order_code = Order.generateOrderCode
      @order.sender_id = @sender.id #thí is sender id
      @order.receiver_id = @receiver.id
      @order.shipper_id = 0 #it mean no shipper
      @order.package_id = @package.id
      @order.ship_type_id = order_params["ship_type_id"]
      @order.order_status_id = 1
      @order.promotion_id = @promotionId
      @order.cod = order_params["cod"].to_i
      @order.created_by = current_user.id #it must be logged in user
      @order.created_at = DateTime.now
      @order.updated_by = current_user.id #itmust be logged in user
      @order.updated_at = DateTime.now
      @order.delete_flag = 0
      
      if order_params["promotion_id"] == "CDVDEFTWE-2017"
        @order.total = 20000
      elsif order_params["promotion_id"] == "CDVDEFTEN-2017"
        @order.total = 10000
      elsif order_params["promotion_id"] == "CDVDEFTHI-2017"
        @order.total = 15000
      elsif order_params["promotion_id"] == "CDVDEF25K-2017"
        @order.total = 25000
      elsif order_params["promotion_id"] == "CDVDEF30K-2017"
        @order.total = 30000
      elsif order_params["promotion_id"] == "CDVDEF35K-2017"
        @order.total = 35000
      elsif order_params["promotion_id"] == "CDVDEF40K-2017"
        @order.total = 40000
      elsif order_params["promotion_id"] == "CDVDEF45K-2017"
        @order.total = 45000
      elsif order_params["promotion_id"] == "CDVDEF50K-2017"
        @order.total = 50000
      else
        from = @sender.address+","+@sender.district+",Đà Nẵng,Việt Nam"
        to = @receiver.address+","+@receiver.district+",Đà Nẵng, Việt Nam"
        
        distanceValue, distanceKm = generateDistanceBetween2points(from, to)
        baseRate = getBaseRate(order_params["ship_type_id"].to_s)
        
        if @promotionId == 0
          @order.total = caculated(baseRate, distanceValue, order_params["ship_type_id"].to_s)
        else
          @order.total = caculated(@price, distanceValue, order_params["ship_type_id"].to_s)
        end
      end
      
      if @order.save
        msg = "Tạo mới đơn hàng " + @order.order_code + " thành công."
        flash[:success] = msg
        
        #add history
        Order.setOrderHistory(@order.id, 1, "Tạo mới đơn hàng",User.find(@order.created_by).profile.full_name)
        
        redirect_to management_orders_path
      else
        flash[:error] = "Tạo đơn hàng Thất bại, vui lòng kiểm tra lại thông tin, hoặc liên hệ người quản lý."
        render new
      end
    end
  end

  def index
    @searchData = ''
    
    if params[:searchData].nil?
      unless is_member?
        @orders = Order.where(delete_flag: 0).order(id: :desc).all
      else
        @orders = Order.where(delete_flag: 0, created_by: current_user.id).order(id: :desc).all
      end
    else
      @searchData = params[:searchData]
      @receiverIds = Receiver.where(delete_flag: 0)
      @receiverIds = @receiverIds.where("lower(phone) LIKE ?", '%'+@searchData.strip.downcase+'%').or(@receiverIds.where("lower(address) LIKE ?", '%'+@searchData.strip.downcase+'%')).select(:id)
      
      unless is_member?
        @orders = Order.where(delete_flag:0, receiver_id: @receiverIds).order(id: :desc).all
      else
        @orders = Order.where(delete_flag:0, created_by: currentUser.id, receiver_id: @receiverIds).order(id: :desc).all
      end
    end
    
  end

  def show
    @order = Order.find(params[:id])
  end

  def edit
    @order = Order.find(params[:id])
    @order.sender_name = @order.sender.name
    @order.sender_phone = @order.sender.phone
    @order.sender_address = @order.sender.address
    @order.sender_district = @order.sender.district
    @order.receiver_name = @order.receiver.name
    @order.receiver_phone = @order.receiver.phone
    @order.receiver_address = @order.receiver.address
    @order.receiver_district = @order.receiver.district
    @order.package_height = @order.package.height
    @order.package_weight = @order.package.weight
    @order.package_width = @order.package.width
    @order.package_note = @order.package.note
    @order.promotion_id = @order.promotion_id == 0 ? "" : @order.promotion.code
  end

  def update
    @order = Order.find(params[:id])
    
    @receiver = Receiver.find(@order.receiver_id)
    @receiver.name = order_params["receiver_name"]
    @receiver.phone = order_params["receiver_phone"]
    @receiver.address = order_params["receiver_address"]
    @receiver.district = order_params["receiver_district"]
    @receiver.city = "Đà Nẵng"
    @receiver.updated_by = current_user.name #itmust be logged in user
    @receiver.updated_at = DateTime.now
    @receiver.delete_flag = 0
    
    @sender = Sender.find(@order.sender_id)
    @sender.name = order_params["sender_name"]
    @sender.phone = order_params["sender_phone"]
    @sender.address = order_params["sender_address"]
    @sender.district = order_params["sender_district"]
    @sender.city = "Đà Nẵng"
    @sender.updated_by = current_user.name #itmust be logged in user
    @sender.updated_at = DateTime.now
    @sender.delete_flag = 0
    
    @package = Package.find(@order.package_id)
    @package.height = order_params["package_height"]
    @package.width = order_params["package_width"]
    @package.weight = order_params["package_weight"]
    @package.note = order_params["package_note"]
    @package.updated_by = current_user.name #itmust be logged in user
    @package.updated_at = DateTime.now
    @package.delete_flag = 0
    
    if @receiver.save && @package.save && @sender.save
      @order = Order.find(params[:id])
      @order.ship_type_id = order_params["ship_type_id"]
      @order.cod = order_params["cod"].to_i
      @order.customer_code = order_params["customer_code"].strip
      @order.payer = order_params["payer"].strip
      
      if order_params["promotion_id"] != ""
        @promote = Promotion.where("code = ? AND count > 0",order_params["promotion_id"]).first
        @order.promotion_id = @promote.id
      end
      
      @order.updated_by = current_user.id #itmust be logged in user
      @order.updated_at = DateTime.now
      @order.delete_flag = 0
      
      if order_params["promotion_id"] == "CDVDEFTWE-2017"
        @order.total = 20000
      elsif order_params["promotion_id"] == "CDVDEFTEN-2017"
        @order.total = 10000
      elsif order_params["promotion_id"] == "CDVDEFTHI-2017"
        @order.total = 15000
      elsif order_params["promotion_id"] == "CDVDEF25K-2017"
        @order.total = 25000
      elsif order_params["promotion_id"] == "CDVDEF30K-2017"
        @order.total = 30000
      elsif order_params["promotion_id"] == "CDVDEF35K-2017"
        @order.total = 35000
      elsif order_params["promotion_id"] == "CDVDEF40K-2017"
        @order.total = 40000
      elsif order_params["promotion_id"] == "CDVDEF45K-2017"
        @order.total = 45000
      elsif order_params["promotion_id"] == "CDVDEF50K-2017"
        @order.total = 50000
      else
      
        from = @sender.address+","+@sender.district+",Đà Nẵng,Việt Nam"
        to = @receiver.address+","+@receiver.district+",Đà Nẵng, Việt Nam"
        
        distanceValue, distanceKm = generateDistanceBetween2points(from, to)
        baseRate = getBaseRate(order_params["ship_type_id"].to_s)
        
        
        @order.total = caculated(baseRate, distanceValue, order_params["ship_type_id"].to_s)
      end
      
      if @order.save
        msg = "Cập nhật đơn hàng " + @order.order_code + " thành công."
        flash[:success] = msg
        redirect_to management_orders_path
      else
        render new
      end
    end
  end

  def destroy
  end
  
  def caculatingOrder
    senderAddress    = params["sender_address"]
    receiverAddress  = params["receiver_address"]
    senderDistrict   = params["sender_district"]
    receiverDistrict = params["receiver_district"]
    ship_type_id     = params["ship_type_id"].to_s
    
    from = senderAddress+","+senderDistrict+",Đà Nẵng,Việt Nam"
    to = receiverAddress+","+receiverDistrict+",Đà Nẵng, Việt Nam"
    
    distanceValue, distanceKm = generateDistanceBetween2points(from, to)
    baseRate = getBaseRate(ship_type_id)
    
    promotionCode = params["promotion_code"]
    
    @price = nil
    @msg = nil
    
    unless promotionCode == ""
      if promotionCode == "CDVDEFTWE-2017"
        @price = 20000
        @msg = "OK"
      elsif promotionCode == "CDVDEFTEN-2017"
        @price = 10000
        @msg = "OK"
      elsif promotionCode == "CDVDEFTHI-2017"
        @price = 15000
        @msg = "OK"
      elsif promotionCode == "CDVDEF25K-2017"
        @price = 25000
        @msg = "OK"
      elsif promotionCode == "CDVDEF30K-2017"
        @price = 30000
        @msg = "OK"
      elsif promotionCode == "CDVDEF35K-2017"
        @price = 35000
        @msg = "OK"
      elsif promotionCode == "CDVDEF40K-2017"
        @price = 40000
        @msg = "OK"
      elsif promotionCode == "CDVDEF45K-2017"
        @price = 45000
        @msg = "OK"
      elsif promotionCode == "CDVDEF50K-2017"
        @price = 50000
        @msg = "OK"
      else
        #check promotion is valid
        @promote = Promotion.where("code = ? AND count > 0",promotionCode).first
        
        unless @promote.nil? #valid
          discount = @promote.discount
          @price = caculated(discount, distanceValue, ship_type_id)
          @msg = "OK"
        else
          @price = caculated(baseRate, distanceValue, ship_type_id)
          @msg = "Failed Promotion"
        end
      end
    else
      @price = caculated(baseRate, distanceValue, ship_type_id)
      @msg = "OK"
    end
    
    render json: {
      "resultData" => {
        "msg" =>  @msg,
        "distanceValue" => distanceValue,
        "distanceText" => distanceKm,
        "totalPrice" => @price
      }
    }
  end
  
  def updateOrderStatus
    orderId = params["orderId"].to_i
    statusId = params["statusId"].to_i
    shipperId = params["shipperId"].to_i
    content = params["content"] 
    updatedBy = params["updatedBy"].to_i
    
    data = "NOK";
    @order = Order.find(orderId)
    @order.order_status_id = statusId
    @order.updated_by = updatedBy
    @order.updated_at = DateTime.now
    
    if shipperId != 0
      @order.shipper_id = shipperId
    end
    
    if @order.save
      
      @orderStatus = OrderStatus.find(statusId)
      
      if shipperId != 0
        content << " - " + User.find(shipperId).profile.full_name
      end
      
      Order.setOrderHistory(orderId, statusId, content, User.find(updatedBy).profile.full_name)
      data = "OK"
    end
    
    #send notify to user
    if statusId == 3 || statusId == 5 || statusId == 9
      userId = @order.created_by.to_i
      @listToken = DeviceToken.where(user_id: userId, delete_flag: 0)
      
      if @listToken.size > 0
        
        #@fcm = FCM.new("AAAAzqwl5ng:APA91bHqs7wLFNYIVzwS_xWr7yU6sqjKeW-RjzoFbA0RoMT9k3jNH8DBxAACxQMJuTYBdkj3rWtknKTt3NtdtS56IV_3Z09pdF6o9LXJU8TTQ8xOSXwna3AvGgSxuaDBEZLTr8j8CFL8")
        
        @FCM_KEY_API  = "AAAAzqwl5ng:APA91bHqs7wLFNYIVzwS_xWr7yU6sqjKeW-RjzoFbA0RoMT9k3jNH8DBxAACxQMJuTYBdkj3rWtknKTt3NtdtS56IV_3Z09pdF6o9LXJU8TTQ8xOSXwna3AvGgSxuaDBEZLTr8j8CFL8"
        @FCM_SEND_API = "https://fcm.googleapis.com/fcm/send"
        registration_ids = []
        
        @listToken.each do |f|
          registration_ids.push(f.token)
        end
        
        # body = {
        #   "to" => registration_ids[0],
        #   "notification" => {
        #     "title": "Thong bao tu CDV - Title",
        #     "body":"content cua thong bao"
        #   }
        # }
        
        body = {
          "registration_ids" => registration_ids,
          "notification" => {
            "title": "Thong bao tu CDV - Title",
            "body":"content cua thong bao"
          }
        }
        
        HTTParty.post(@FCM_SEND_API,
        { 
          :body => body.to_json,
          :headers => { 'Content-Type' => 'application/json', 'Authorization' => 'key=AAAAzqwl5ng:APA91bHqs7wLFNYIVzwS_xWr7yU6sqjKeW-RjzoFbA0RoMT9k3jNH8DBxAACxQMJuTYBdkj3rWtknKTt3NtdtS56IV_3Z09pdF6o9LXJU8TTQ8xOSXwna3AvGgSxuaDBEZLTr8j8CFL8'}
        })
        
      end
    end
    
    render json: {
        "resultData": {
          "status": data
        }
      }
  end
  
  def importView
    
  end
  
  def import
    uploaded_file = params[:file]
    fileName = uploaded_file.original_filename
    #check the upload file is excel file
    if fileName.end_with?(".xls") || fileName.end_with?(".xlsx")
      #starting to read xls file and upload to server
      file_path      = params[:file].tempfile
      file_extension = File.extname params[:file].original_filename
      if file_extension.eql? ".xlsx"
        file_data       = Roo::Spreadsheet.open(file_path)
        @data            = file_data.sheet 0
        @first_row_index = 1+8
        @originalRow     = 1+8
      else
        file_data       = Spreadsheet.open(file_path)
        @data            = file_data.worksheet 0
        @first_row_index = 0+8
        @originalRow     = 0+8
      end
      
      @isContinue = true
      
      while @data.row(@first_row_index)[0].nil? == false && @isContinue do
        dataRow = @data.row(@first_row_index)
        @isContinue = validateOrder?(dataRow)
        @first_row_index += 1
      end
      
      while @data.row(@originalRow)[0].nil? == false && @isContinue do
        dataRow = @data.row(@originalRow)
        @isContinue = addOrder?(dataRow)
        @originalRow += 1
      end
      
      if @isContinue
        flash[:success] = 'Tạo đơn hàng Thành Công'
      end
    else
      flash[:error] = 'File không hợp lệ, Vui lòng kiểm tra lại. File phải là định dạng Excel.'
    end
    redirect_to management_orders_path && return
  end
  
  private
    
    def order_params
      params.require(:order).permit(
        :receiver_name, :receiver_phone, :receiver_address, 
        :customer_code, :payer,
        :receiver_district, :receiver_city, :package_height, 
        :package_weight, :package_width, :package_note, 
        :ship_type_id, :promotion_id, :order_status_id,
        :cod, :order_code, :total,
        :created_by, :updated_by, :delete_flag,
        :sender_name, :sender_phone, :sender_address, :sender_id,
        :sender_district, :sender_city)
    end
    
    def generateDistanceBetween2points(from, to)
      #GG API KEY - khovideos1@gmail.com
      @API_KEY = "AIzaSyB2O1_Piue0e0oEOUNFI3ONKb5O6dodPXE"
      @MAP_API = "https://maps.googleapis.com/maps/api/directions/json?"
      
      prms = URI.encode_www_form([["origin", from], ["destination", to],["key", @API_KEY],["mode","json"],["language","vi"]])
      @MAP_API << prms
      
      response = HTTParty.get(@MAP_API)
      response.parsed_response


      if response["status"] == "OK"
        distanceValue = response["routes"][0]["legs"][0]["distance"]["value"].to_i
        distanceKm = response["routes"][0]["legs"][0]["distance"]["text"].to_s
      end
      
      return distanceValue, distanceKm
    end
    
    def generateDistrictFromAddress(addr)
      @GEO_API = "https://maps.googleapis.com/maps/api/geocode/json?"
      prms = URI.encode_www_form([["address", addr]])
      
      @GEO_API << prms
      
      response = HTTParty.get(@GEO_API)
      response.parsed_response
      
      if response["status"] == "OK"
        address = response["results"][0]["formatted_address"].to_s
      end
      
      return address
    end
    
    def getBaseRate(ship_type_id)
      if ship_type_id == "1" #giao thường
        baseRate = 10000
      elsif ship_type_id == "2" #giao nhanh
        baseRate = 20000
      else #giao siêu tốc
        baseRate = 30000
      end
      return baseRate
    end
    
    def caculated(baseRate, km, type)
      value = 0
      km = km / 1000.0
      
      if km % 1 != 0
        km = km.to_i + 1
      end
      
      if km > 10
        if type == "1"
          value = baseRate + ((km - 10) * 3000)
        elsif type == "2"
          value = baseRate + ((km - 10) * 4000)
        else
          value = baseRate + ((km - 10) * 5000)
        end
      else
        value = baseRate
      end
      return value
    end
    
    def is_member?
        self.current_user.role.name == "Người Dùng"
    end
    
    def isPromotionValid?(promotionCode)
      @promot = Promotion.where("code = ? AND count > 0",promotionCode).all
      
      if @promot.size > 0
        return true
      end
      return false
    end
    
    def validateOrder?(data)
      num = data[0] 
      senderName = data[1].to_s
      senderAddress = data[2].to_s
      senderDistrict = data[3].to_s
      senderPhone = data[4].to_s
      receiverName = data[5].to_s
      receiverAddress = data[6].to_s
      receiverDistrict = data[7].to_s
      receiverPhone = data[8].to_s
      packageHeight = data[9].to_s
      packageWidth = data[10].to_s
      packageWeight = data[11].to_s
      packageNote = data[12].nil? ? "" : data[12].to_s
      shipType = data[13].to_s
      cod = data[14].nil? ? 0 : data[14].to_i
      promotion = data[15].nil? ? "" : data[15].to_s
      
      #validate sender information
      if senderName == ""
        flash[:error] = 'Tên người gởi của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
      
      if senderAddress == ""
        flash[:error] = 'Địa chỉ người gởi của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
      
      if senderPhone == ""
        flash[:error] = 'SĐT người gởi của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
      
      if senderDistrict == ""
        flash[:error] = 'Tên Quận người gởi của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
      
      #validate receiver information
      if receiverName == ""
        flash[:error] = 'Tên người nhận của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
      
      if receiverAddress == ""
        flash[:error] = 'Địa chỉ người nhận của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
      
      if receiverPhone == ""
        flash[:error] = 'SĐT người nhận của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
      
      if receiverDistrict == ""
        flash[:error] = 'Tên Quận người nhận của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
      
      #validate package information
      if packageHeight == ""
        flash[:error] = 'Chiều cao gói hàng của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
      
      if packageWidth == ""
        flash[:error] = 'Chiều rộng gói hàng của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
      
      if packageWeight == ""
        flash[:error] = 'Cân Nặng gói hàng của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
      
      #validate order information
      
      if shipType == ""
        flash[:error] = 'Kiểu vận chuyển của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      elsif shipType.downcase != "chuyển thường" && shipType.downcase != "chuyển nhanh" && shipType.downcase != "chuyển siêu tốc"
        flash[:error] = 'Kiểu vận chuyển của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
        return false
      end
      
      unless Order.validUserForOrder?(current_user.id)
        flash[:userInfo] = 'Vui lòng cập nhật đầy đủ thông tin tại <a href="/management/my-profile"> ĐÂY</a>, trước khi tạo đơn hàng.'
        return false
      end
      
      unless promotion == ""
        #check promotion is valid
        @promote = Promotion.where("code = ? AND count > 0",promotion).first
        unless @promote.nil? #valid
          @price = @promote.discount
          @promotionId = @promote.id
        else
          flash[:error] = 'Mã khuyến mãi của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
          return false
        end
      end
      
      return true
    end
    
    def addOrder?(data)
      num = data[0] 
      senderName = data[1].to_s
      senderAddress = data[2].to_s
      senderDistrict = data[3].to_s
      senderPhone = data[4].to_s
      receiverName = data[5].to_s
      receiverAddress = data[6].to_s
      receiverDistrict = data[7].to_s
      receiverPhone = data[8].to_s
      packageHeight = data[9].to_s
      packageWidth = data[10].to_s
      packageWeight = data[11].to_s
      packageNote = data[12].nil? ? "" : data[12].to_s
      shipType = data[13].to_s
      cod = data[14].nil? ? 0 : data[14].to_i
      promotion = data[15].nil? ? "" : data[15].to_s
      ship_type_id = ShipType.find_by(name: shipType).id
      
      unless promotion == ""
        #check promotion is valid
        @promote = Promotion.where("code = ? AND count > 0",promotion).first
        unless @promote.nil? #valid
          @price = @promote.discount
          @promotionId = @promote.id
        else
          flash[:error] = 'Mã khuyến mãi của đơn hàng '+num.to_s+' không hợp lệ, Vui lòng kiểm tra lại.'
          return false
        end
      else
        @price = 0
        @promotionId = 0
      end
      
      @sender = Sender.new
      @sender.name = senderName
      @sender.phone = senderPhone
      @sender.address = senderAddress
      @sender.district = senderDistrict
      @sender.city = "Đà Nẵng"
      @sender.created_by = current_user.name #it must be logged in user
      @sender.created_at = DateTime.now
      @sender.updated_by = current_user.name #itmust be logged in user
      @sender.updated_at = DateTime.now
      @sender.delete_flag = 0
      
      @receiver = Receiver.new
      @receiver.name = receiverName
      @receiver.phone = receiverPhone
      @receiver.address = receiverAddress
      @receiver.district = receiverDistrict
      @receiver.city = "Đà Nẵng"
      @receiver.created_by = current_user.name #it must be logged in user
      @receiver.created_at = DateTime.now
      @receiver.updated_by = current_user.name #itmust be logged in user
      @receiver.updated_at = DateTime.now
      @receiver.delete_flag = 0
      
      @package = Package.new
      @package.height = packageHeight
      @package.width = packageWidth
      @package.weight = packageWeight
      @package.note = packageNote
      @package.created_by = current_user.name #it must be logged in user
      @package.created_at = DateTime.now
      @package.updated_by = current_user.name #itmust be logged in user
      @package.updated_at = DateTime.now
      @package.delete_flag = 0
      
      #save @receiver, @sender, @package info
      if @receiver.save && @package.save && @sender.save
        @order = Order.new
        @order.order_code = Order.generateOrderCode
        @order.sender_id = @sender.id #thí is sender id
        @order.receiver_id = @receiver.id
        @order.shipper_id = 0 #it mean no shipper
        @order.package_id = @package.id
        @order.ship_type_id = ship_type_id
        @order.order_status_id = 1
        @order.promotion_id = @promotionId
        @order.cod = cod
        @order.created_by = current_user.id #it must be logged in user
        @order.created_at = DateTime.now
        @order.updated_by = current_user.id #itmust be logged in user
        @order.updated_at = DateTime.now
        @order.delete_flag = 0
        
        if promotion == "CDVDEFTWE-2017"
          @order.total = 20000
        elsif promotion == "CDVDEFTEN-2017"
          @order.total = 10000
        elsif promotion == "CDVDEFTHI-2017"
          @order.total = 15000
        elsif promotion == "CDVDEF25K-2017"
          @order.total = 25000
        elsif promotion == "CDVDEF30K-2017"
          @order.total = 30000
        elsif promotion == "CDVDEF35K-2017"
          @order.total = 35000
        elsif promotion == "CDVDEF40K-2017"
          @order.total = 40000
        elsif promotion == "CDVDEF45K-2017"
          @order.total = 45000
        elsif promotion == "CDVDEF50K-2017"
          @order.total = 50000
        else
          from = @sender.address+","+@sender.district+",Đà Nẵng,Việt Nam"
          to = @receiver.address+","+@receiver.district+",Đà Nẵng, Việt Nam"
          
          distanceValue, distanceKm = generateDistanceBetween2points(from, to)
          baseRate = getBaseRate(ship_type_id.to_s)
          
          if @promotionId == 0
            @order.total = caculated(baseRate, distanceValue, ship_type_id.to_s)
          else
            @order.total = caculated(@price, distanceValue, ship_type_id.to_s)
          end
        end
        
        if @order.save
          #add history
          Order.setOrderHistory(@order.id, 1, "Tạo mới đơn hàng",User.find(@order.created_by).profile.full_name)
          return true
        else
          flash[:error] = "Tạo đơn hàng Thất bại, vui lòng kiểm tra lại thông tin, hoặc liên hệ người quản lý."
          return false
        end
      end
    end
end
