class Management::CustomerTypesController < Management::ApplicationController
  before_filter :verify_logged
  
  def new
    @customerType = CustomerType.new
  end

  def create
    @customerType = CustomerType.new(customerType_params)
    @customerType.created_by = current_user.name
    @customerType.updated_by = current_user.name
    @customerType.delete_flag = 0
    
    if @customerType.save
      flash[:success] = 'Tạo mới thành công'
      redirect_to management_customer_types_path
    else
      flash[:success] = 'Tạo mới thất bại, vui lòng kiểm tra lại'
      render 'new'
    end
  end

  def index
    @customerTypes = CustomerType.where(delete_flag: 0).all
  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
  end
  
  private
    def customerType_params
      params.require(:customer_type).permit(:name, :description, :created_by, :updated_by, :delete_flag)
    end
end
