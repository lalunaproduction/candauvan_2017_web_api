// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require management/jquery
//= require jquery_ujs
//= require jquery.turbolinks
//= require management/bootstrap
//= require management/fastclick
//= require management/nprogress
//= require management/chart
//= require management/gauge
//= require management/bootstrap-progressbar
//= require management/icheck
//= require management/skycons
//= require management/jqueryflot
//= require management/jqueryflotpie
//= require management/jqueryflottime
//= require management/jqueryflotstack
//= require management/jqueryflotresize
//= require management/jqueryflotorderBars
//= require management/jqueryflotspline
//= require management/curvedLines
//= require management/date
//= require management/jqueryvmap
//= require management/jqueryvmaporld
//= require management/jqueryvmapsampledata
//= require management/moment
//= require management/daterangepicker
//= require management/validator
//= require management/fullcalendar
//= require management/jquerysmartwizard
//= require management/dropzone
//= require ckeditor/init
//= require select2-full
//= require management/custom
//= require_tree .