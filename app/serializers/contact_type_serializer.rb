class ContactTypeSerializer < ActiveModel::Serializer
  attributes :id, :name, :created_by, :updated_by, :delete_flag, :created_at, :updated_at
end
