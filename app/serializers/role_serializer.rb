class RoleSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :created_by, :updated_by, :delete_flag, :created_at, :updated_at
end
