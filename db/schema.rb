# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170804091505) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contact_people", force: :cascade do |t|
    t.integer  "contact_person_type_id"
    t.string   "name"
    t.string   "phone"
    t.string   "address"
    t.string   "district"
    t.string   "city"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "contact_person_types", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.boolean  "delete_flag"
    t.string   "created_by"
    t.string   "updated_by"
  end

  create_table "customer_types", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.boolean  "delete_flag"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "device_tokens", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "token"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "order_histories", force: :cascade do |t|
    t.integer  "order_id"
    t.string   "action"
    t.string   "content"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "order_statuses", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "orders", force: :cascade do |t|
    t.string   "order_code"
    t.integer  "receiver_id"
    t.integer  "shipper_id"
    t.integer  "package_id"
    t.integer  "ship_type_id"
    t.integer  "cod"
    t.integer  "order_status_id"
    t.integer  "promotion_id"
    t.float    "total"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "sender_id"
    t.string   "customer_code"
    t.string   "payer"
  end

  create_table "packages", force: :cascade do |t|
    t.string   "height"
    t.string   "width"
    t.string   "weight"
    t.string   "image_url"
    t.string   "note"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "full_name"
    t.string   "address"
    t.string   "district"
    t.string   "city"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "promotions", force: :cascade do |t|
    t.string   "code"
    t.integer  "discount"
    t.datetime "discount_from"
    t.datetime "discount_to"
    t.integer  "count"
    t.string   "description"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "receivers", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "address"
    t.string   "district"
    t.string   "city"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "senders", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "address"
    t.string   "district"
    t.string   "city"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "ship_types", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "created_by"
    t.string   "updated_by"
    t.boolean  "delete_flag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.integer  "role_id"
    t.string   "email"
    t.string   "phone"
    t.string   "encrypt_password"
    t.string   "salt"
    t.string   "provider"
    t.string   "created_by"
    t.string   "updated_by"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.boolean  "delete_flag"
    t.string   "token"
    t.integer  "customer_type_id"
  end

end
