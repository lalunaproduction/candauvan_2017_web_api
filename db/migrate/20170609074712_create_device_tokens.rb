class CreateDeviceTokens < ActiveRecord::Migration[5.0]
  def change
    create_table :device_tokens do |t|
      t.integer :user_id
      t.string :token
      t.boolean :delete_flag

      t.timestamps
    end
  end
end
