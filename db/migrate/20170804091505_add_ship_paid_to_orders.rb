class AddShipPaidToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :payer, :string
  end
end
