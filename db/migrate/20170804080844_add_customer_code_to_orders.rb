class AddCustomerCodeToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :customer_code, :string
  end
end
