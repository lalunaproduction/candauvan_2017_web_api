class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.string :order_code
      t.integer :user_id
      t.integer :receiver_id
      t.integer :shipper_id
      t.integer :package_id
      t.integer :ship_type_id
      t.integer :cod
      t.integer :order_status_id
      t.integer :promotion_id
      t.float :total
      t.string :created_by
      t.string :updated_by
      t.boolean :delete_flag

      t.timestamps
    end
  end
end
