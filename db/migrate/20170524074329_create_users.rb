class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.integer :role_id
      t.string :email
      t.string :phone
      t.string :encrypt_password
      t.string :salt
      t.string :provider
      t.string :created_by
      t.string :updated_by
      t.string :delete_flag_boolean

      t.timestamps
    end
  end
end
