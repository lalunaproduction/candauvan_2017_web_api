class CreatePromotions < ActiveRecord::Migration[5.0]
  def change
    create_table :promotions do |t|
      t.string :code
      t.integer :discount
      t.datetime :discount_from
      t.datetime :discount_to
      t.integer :count
      t.string :description
      t.string :created_by
      t.string :updated_by
      t.boolean :delete_flag

      t.timestamps
    end
  end
end
