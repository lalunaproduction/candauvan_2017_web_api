class CreateOrderHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :order_histories do |t|
      t.integer :order_id
      t.string :action
      t.string :content
      t.string :created_by
      t.string :updated_by
      t.boolean :delete_flag

      t.timestamps
    end
  end
end
