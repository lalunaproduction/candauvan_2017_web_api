class ChangeUserIdToSenderId < ActiveRecord::Migration[5.0]
  def change
    remove_column :orders, :user_id
    add_column :orders, :sender_id, :integer
  end
end
