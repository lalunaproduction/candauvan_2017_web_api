class ChangeUserDeleteFlag < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :delete_flag_boolean
    add_column :users, :delete_flag, :boolean
  end
end
