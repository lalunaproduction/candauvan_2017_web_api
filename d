               List of relations
 Schema |         Name         | Type  | Owner  
--------+----------------------+-------+--------
 public | ar_internal_metadata | table | ubuntu
 public | contact_people       | table | ubuntu
 public | contact_person_types | table | ubuntu
 public | order_statuses       | table | ubuntu
 public | orders               | table | ubuntu
 public | packages             | table | ubuntu
 public | profiles             | table | ubuntu
 public | promotions           | table | ubuntu
 public | roles                | table | ubuntu
 public | schema_migrations    | table | ubuntu
 public | ship_types           | table | ubuntu
 public | users                | table | ubuntu
(12 rows)

